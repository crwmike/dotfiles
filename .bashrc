### EXPORT ###
export HISTCONTROL=ignoreboth:erasedups
export PAGER='less'
export EDITOR="nvim"
export SUDO_EDITOR="nvim"
export VISUAL="nvim"

#Ibus settings if you need them
#type ibus-setup in terminal to change settings and start the daemon
#delete the hashtags of the next lines and restart
#export GTK_IM_MODULE=ibus
#export XMODIFIERS=@im=dbus
#export QT_IM_MODULE=ibus
#brightnessctl set 50% &

PS1='[\u@\h \W]\$ '
export PS1="\[\e[36m\]\u\[\e[m\]@\[\e[32m\]\h\[\e[m\] \[\e[34m\]\w\[\e[m\] \[\e[33m\]\\$\[\e[m\] "

# If not running interactively, don't do anything
[[ $- != *i* ]] && return


if [ -d "$HOME/.bin" ] ;
  then PATH="$HOME/.bin:$PATH"
fi

if [ -d "$HOME/bin" ] ;
  then PATH="$HOME/bin:$PATH"
fi

if [ -d "$HOME/.local/bin" ] ;
  then PATH="$HOME/.local/bin:$PATH"
fi

#ignore upper and lowercase when TAB completion
bind "set completion-ignore-case on"

### ALIASES ###

#list
alias ls='ls --color=auto'
#alias la='ls -a'
#alias ll='ls -alFh'
#alias l='ls'
alias l.="ls -A | egrep '^\.'"
alias listdir="ls -d */ > list"

#pacman
alias sps='sudo pacman -S'
alias spr='sudo pacman -R'
alias sprs='sudo pacman -Rs'
alias sprdd='sudo pacman -Rdd'
alias spqo='sudo pacman -Qo'
alias spsii='sudo pacman -Sii'

#other
alias vi='nvim'
alias calc='wcalc -C'
alias loadrc='source ~/.bashrc'

# show the list of packages that need this package - depends mpv as example
function_depends()  {
    search=$(echo "$1")
    sudo pacman -Sii $search | grep "Required" | sed -e "s/Required By     : //g" | sed -e "s/  /\n/g"
    }

alias depends='function_depends'

#fix obvious typo's
alias cd..='cd ..'
alias pdw='pwd'
alias udpate='sudo pacman -Syyu'
alias upate='sudo pacman -Syyu'
alias updte='sudo pacman -Syyu'
alias updqte='sudo pacman -Syyu'
alias upqll='paru -Syu --noconfirm'
alias upal='paru -Syu --noconfirm'
alias vu='nvim'

# # ex = EXtractor for all kinds of archives
# # usage: ex <file>
ex ()
{
  if [ -f $1 ] ; then
    case $1 in
      *.tar.bz2)   tar xjf $1   ;;
      *.tar.gz)    tar xzf $1   ;;
      *.bz2)       bunzip2 $1   ;;
      *.rar)       unrar x $1   ;;
      *.gz)        gunzip $1    ;;
      *.tar)       tar xf $1    ;;
      *.tbz2)      tar xjf $1   ;;
      *.tgz)       tar xzf $1   ;;
      *.zip)       unzip $1     ;;
      *.Z)         uncompress $1;;
      *.7z)        7z x $1      ;;
      *.deb)       ar x $1      ;;
      *.tar.xz)    tar xf $1    ;;
      *.tar.zst)   tar xf $1    ;;
      *)           echo "'$1' cannot be extracted via ex()" ;;
    esac
  else
    echo "'$1' is not a valid file"
  fi
}


# ------------------------------------------------------------------------------
# ---- color less-manpages                                                  ----
# ------------------------------------------------------------------------------
export LESS_TERMCAP_md=$'\e[38;5;111m'       # blink mode     (section)
export LESS_TERMCAP_so=$'\e[30m\e[48;5;116m' # reverse mode
export MANPAGER="less -R --use-color -Dd+b -Du+m"
export MANROFFOPT="-P -c"
export MANWIDTH=80
#-------------------------------------------------------------------------------
#export LESSOPEN="| /usr/share/source-highlight/src-hilite-lesspipe.sh %s"
export LESSHISTFILE="/home/mike/.config/less/history"
export MICRO_TRUECOLOR=1
export LS_COLORS=$LS_COLORS:"ex=38;5;86:di=38;5;111:ln=0;38;5;117"
export EXA_COLORS='di=38;5;111:hd=4;37:in=37:ln=38;5;117:ur=32:uw=32:ux=32:gr=34:gw=34:gx=34:tr=35:tw=35:tx=35:sn=38;5;224:sb=37:lc=37:lm=1;35:uu=38;5;183:gu=38;5;183:gn=33:un=33:da=38;5;37'

#pamac
alias pamac-unlock="sudo rm /var/tmp/pamac/dbs/db.lock"

#moving your personal files and folders from /personal to ~
alias personal='cp -Rf /personal/* ~'

#create a file called .bashrc-personal and put all your personal aliases
#in there. They will not be overwritten by skel.

[[ -f ~/.bashrc-personal ]] && . ~/.bashrc-personal

#STARSHIP
eval "$(starship init bash)"


# reporting tools - install when not installed
#fastfetch
#neofetch
#screenfetch
#alsi
#paleofetch
#fetch
#hfetch
#sfetch
#ufetch
#ufetch-arco
#pfetch
#sysinfo
#sysinfo-retro
#cpufetch
#colorscript random
#hyfetch
task list due.before:tomorrow
remind ~/.reminders

# load ~/.aliases
if [ -f ~/.aliases ]; then
    . ~/.aliases
fi

[ -f ~/.fzf.bash ] && source ~/.fzf.bash

unalias cd
