#!/bin/bash

function run {
    if (command -v $1 && ! pgrep $1);
  then
    $@&
  fi
}
run "nm-applet"
#run "/usr/bin/compton --config ~/.config/compton/compton.conf &"
run "variety"
run "emacs --daemon &"
run "xfce4-power-manager"
#run "numlockx on"
#you can set wallpapers in themes as well
#feh --bg-fill /usr/share/backgrounds/arcolinux/arco-wallpaper.jpg &
run "udiskie -ans &"
run "numlockx on"
run "dropbox start"
run "~/usr/bin/dunst &"
run "/usr/bin/touchpad-indicator"
#  "xfce4-power-manager --restart"
