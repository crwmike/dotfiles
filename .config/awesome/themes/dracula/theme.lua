---------------------------
-- Dracula awesome theme --
---------------------------

local theme_assets = require("beautiful.theme_assets")
local xresources = require("beautiful.xresources")
local dpi = xresources.apply_dpi

local gfs = require("gears.filesystem")
local themes_path = gfs.get_themes_dir()

local theme = {}
theme.dir   = os.getenv("HOME") .. "/.config/awesome/themes/dracula"

theme.font          = "sans 11"

theme.bg_normal     = "#282936"
theme.bg_focus      = "#526294"
theme.bg_urgent     = "#282936"
theme.bg_minimize   = "#282936"
theme.bg_systray    = theme.bg_normal

theme.fg_normal     = "#c8c8d2"
theme.fg_focus      = "#ffffff"
theme.fg_urgent     = "#50fa7b"
theme.fg_minimize   = "#ffb86c"

theme.useless_gap   = dpi(4)
theme.border_width  = dpi(1)
theme.border_normal = "#44475a"
theme.border_focus  = "#bd93f9"
theme.border_marked = "#ff79c6"

-- There are other variable sets
-- overriding the default one when
-- defined, the sets are:
-- taglist_[bg|fg]_[focus|urgent|occupied|empty|volatile]
-- tasklist_[bg|fg]_[focus|urgent]
-- titlebar_[bg|fg]_[normal|focus]
-- tooltip_[font|opacity|fg_color|bg_color|border_width|border_color]
-- mouse_finder_[color|timeout|animate_timeout|radius|factor]
-- prompt_[fg|bg|fg_cursor|bg_cursor|font]
-- hotkeys_[bg|fg|border_width|border_color|shape|opacity|modifiers_fg|label_bg|label_fg|group_margin|font|description_font]
-- Example:
--theme.taglist_bg_focus = "#ff0000"

-- Generate taglist squares:
local taglist_square_size = dpi(4)
theme.taglist_squares_sel = theme_assets.taglist_squares_sel(
    taglist_square_size, theme.fg_normal
)
theme.taglist_squares_unsel = theme_assets.taglist_squares_unsel(
    taglist_square_size, theme.fg_normal
)

-- Variables set for theming notifications:
-- notification_font
-- notification_[bg|fg]
-- notification_[width|height|margin]
-- notification_[border_color|border_width|shape|opacity]

-- Variables set for theming the menu:
-- menu_[bg|fg]_[normal|focus]
-- menu_[border_color|border_width]
theme.awesome_icon      = theme.dir .. "/icons/awesome.png"
theme.menu_submenu_icon = theme.dir .. "/icons/submenu.png"
theme.menu_fg_normal    = "#f8f8f2"
theme.menu_fg_focus     = "#ffffff"
theme.menu_bg_normal    = "#34374a"
theme.menu_bg_focus     = "#bd93f9"
theme.menu_height       = dpi(20)
theme.menu_width        = dpi(200)

-- You can add as many variables as
-- you wish and access them by using
-- beautiful.variable in your rc.lua
--theme.bg_widget = "#cc0000"

-- Define the image to load
theme.titlebar_close_button_normal = theme.dir  .. "/titlebar/close_normal.png"
theme.titlebar_close_button_focus  = theme.dir  .. "/titlebar/close_focus.png"

theme.titlebar_minimize_button_normal = theme.dir .. "/titlebar/minimize_normal.png"
theme.titlebar_minimize_button_focus  = theme.dir .. "/titlebar/minimize_focus.png"

theme.titlebar_ontop_button_normal_inactive = theme.dir .. "/titlebar/ontop_normal_inactive.png"
theme.titlebar_ontop_button_focus_inactive  = theme.dir .. "/titlebar/ontop_focus_inactive.png"
theme.titlebar_ontop_button_normal_active = theme.dir   .. "/titlebar/ontop_normal_active.png"
theme.titlebar_ontop_button_focus_active  = theme.dir   .. "/titlebar/ontop_focus_active.png"

theme.titlebar_sticky_button_normal_inactive = theme.dir .. "/titlebar/sticky_normal_inactive.png"
theme.titlebar_sticky_button_focus_inactive  = theme.dir .. "/titlebar/sticky_focus_inactive.png"
theme.titlebar_sticky_button_normal_active = theme.dir   .. "/titlebar/sticky_normal_active.png"
theme.titlebar_sticky_button_focus_active  = theme.dir   .. "/titlebar/sticky_focus_active.png"

theme.titlebar_floating_button_normal_inactive = theme.dir .. "/titlebar/floating_normal_inactive.png"
theme.titlebar_floating_button_focus_inactive  = theme.dir .. "/titlebar/floating_focus_inactive.png"
theme.titlebar_floating_button_normal_active = theme.dir   .. "/titlebar/floating_normal_active.png"
theme.titlebar_floating_button_focus_active  = theme.dir   .. "/titlebar/floating_focus_active.png"

theme.titlebar_maximized_button_normal_inactive = theme.dir .. "/titlebar/maximized_normal_inactive.png"
theme.titlebar_maximized_button_focus_inactive  = theme.dir .. "/titlebar/maximized_focus_inactive.png"
theme.titlebar_maximized_button_normal_active = theme.dir   .. "/titlebar/maximized_normal_active.png"
theme.titlebar_maximized_button_focus_active  = theme.dir   .. "/titlebar/maximized_focus_active.png"

-- theme.wallpaper = themes_path.."default/background.png"

-- You can use your own layout icons like this:
theme.layout_fairh = theme.dir      .. "/layouts/fairhw.png"
theme.layout_fairv = theme.dir      .. "/layouts/fairvw.png"
theme.layout_floating  = theme.dir  .. "/layouts/floatingw.png"
theme.layout_magnifier = theme.dir  .. "/layouts/magnifierw.png"
theme.layout_max = theme.dir        .. "/layouts/maxw.png"
theme.layout_fullscreen = theme.dir .. "/layouts/fullscreenw.png"
theme.layout_tilebottom = theme.dir .. "/layouts/tilebottomw.png"
theme.layout_tileleft   = theme.dir .. "/layouts/tileleftw.png"
theme.layout_tile = theme.dir       .. "/layouts/tilew.png"
theme.layout_tiletop = theme.dir    .. "/layouts/tiletopw.png"
theme.layout_spiral  = theme.dir    .. "/layouts/spiralw.png"
theme.layout_dwindle = theme.dir    .. "/layouts/dwindlew.png"
theme.layout_cornernw = theme.dir   .. "/layouts/cornernww.png"
theme.layout_cornerne = theme.dir   .. "/layouts/cornernew.png"
theme.layout_cornersw = theme.dir   .. "/layouts/cornersww.png"
theme.layout_cornerse = theme.dir   .. "/layouts/cornersew.png"

-- Generate Awesome icon:
-- theme.awesome_icon = theme_assets.awesome_icon(
--     theme.menu_height, theme.bg_focus, theme.fg_focus
-- )

-- Define the icon theme for application icons. If not set then the icons
-- from /usr/share/icons and /usr/share/icons/hicolor will be used.
theme.icon_theme = nil

return theme

-- vim: filetype=lua:expandtab:shiftwidth=4:tabstop=8:softtabstop=4:textwidth=80
