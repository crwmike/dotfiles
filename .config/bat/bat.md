ALIASES:

```
bat='bat -n --theme="Catppuccin-macchiato"'
cat='bat -n --paging=never'
```
## Catppuccin for Bat 

[catppuccin/bat](https://github.com/catppuccin/bat)  

1. Create a theme folder in bat's configuration directory by running:
   ```
   mkdir -p "$(bat --config-dir)/themes"
   ```

2. Copy the theme files from this repository:
   ```
   wget -P "$(bat --config-dir)/themes" https://github.com/catppuccin/bat/raw/main/themes/Catppuccin%20Latte.tmTheme
   wget -P "$(bat --config-dir)/themes" https://github.com/catppuccin/bat/raw/main/themes/Catppuccin%20Frappe.tmTheme
   wget -P "$(bat --config-dir)/themes" https://github.com/catppuccin/bat/raw/main/themes/Catppuccin%20Macchiato.tmTheme
   wget -P "$(bat --config-dir)/themes" https://github.com/catppuccin/bat/raw/main/themes/Catppuccin%20Mocha.tmTheme
   ```

3. Rebuild bat's cache:
   ```
   bat cache --build 
   ```

4. Run `bat --list-themes`, and check if the themes are present in the list.


### Configuration file

Edit your configuration file, located at `bat --config-file` (usually `~/.config/bat/config`):
```
--theme="Catppuccin Mocha"
```

### Environment variable

You can alternatively use the BAT_THEME environment variable. Export the
environment variable inside your shell's configuration file:
`export BAT_THEME="Catppuccin Mocha"`.

The method to export the variable depends on your shell.
