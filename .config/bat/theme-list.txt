1337
Catppuccin-frappe
Catppuccin-latte
Catppuccin-macchiato
Catppuccin-mocha
Coldark-Cold
Coldark-Dark
DarkNeon
Dracula
GitHub
Monokai Extended (default dark)
Monokai Extended Bright
Monokai Extended Light (default light)
Monokai Extended Origin
Nord
OneHalfDark
OneHalfLight
Solarized (dark)
Solarized (light)
Sublime Snazzy
TwoDark
Visual Studio Dark+
ansi
base16
base16-256
gruvbox-dark
gruvbox-light
zenburn
