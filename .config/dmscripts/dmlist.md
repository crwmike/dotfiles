# dmscripts

* dm-bookman    - Search your qutebrowser bookmarks, quickmarks and history urls.
* dm-colpick    - Copy a color's hex value to your clipboard
* dm-confedit   - Choose from a list of configuration files to edit.    // ~/bin/dmconf
* dm-hub        - A hub from where you can run all the scripts from.
* dm-ip         - Get IP of interface or external IP
* dm-kill       - Search for a process to kill.                         // ~/bin/dmkill
* dm-logout     - Logout, shutdown, reboot or lock screen.              // ~/bin/dmpower
* dm-maim       - A GUI to maim using dmenu.
* dm-man        - Search for a manpage or get a random one.
* dm-music      - Dmenu as your music player
* dm-pacman     - A software store using dmenu
* dm-reddit     - Dmenu as a reddit viewer using reddio. STILL A WORK IN PROGRESS
* dm-setbg      - A wallpaper setting utility using dmenu, xwallpaper and sxiv
* dm-sounds     - Choose an ambient background to play.                 // ~/bin/dmsounds
* dm-usbmount   - mount/unmount usb drives using dmenu. No fancy daemon required
* dm-websearch  - Search various search engines (inspired by surfraw).
* dm-wifi       - Connect to wifi using dmenu.
* dm-youtube    - Youtube subscriptions without an account or the API tying you down.
* _dm-helper.sh Helper scripts adding functionality to other scripts
