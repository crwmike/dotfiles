# GRUB Edit

In the terminal:
```
sudo vi /etc/default/grub
```
Save the file and then in the terminal.
```
sudo update-grub
```
Reboot.


# GRUB Background Color
```
sudo vi /usr/share/plymouth/themes/ubuntu-budgie-logo/ubuntu-logo.grub
```
Edit Backgroud_color, save and exit.
In the terminal:
```
sudo update-grub
```
Reboot.


