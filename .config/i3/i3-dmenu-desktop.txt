i3 Perl documentation
https://build.i3wm.org/docs/i3-dmenu-desktop.html

edit line 47 of /usr/bin/i3-dmenu-desktop

    my $dmenu_cmd = 'dmenu -i -nb #282936 -nf #cbcbef -sb #af87ff -sf #000000';
    my $dmenu_cmd = 'dmenu -i -nb #282936 -nf #af87ff -sb #5fff87 -sf #000000';
    my $dmenu_cmd = 'dmenu -i -nb #282936 -nf #87d7ff -sb #5fff87 -sf #000000';


NAME
    i3-dmenu-desktop - run .desktop files with dmenu
SYNOPSIS
    i3-dmenu-desktop [--dmenu='dmenu -i'] [--entry-type=name]

DESCRIPTION
i3-dmenu-desktop is a script which extracts the (localized) name from application .desktop files, offers the user a choice via dmenu(1) and then starts the chosen application via i3 (for startup notification support). The advantage of using .desktop files instead of dmenu_run(1) is that dmenu_run offers all binaries in your $PATH, including non-interactive utilities like "sed". Also, .desktop files contain a proper name, information about whether the application runs in a terminal and whether it supports startup notifications.

