# LYNX

#### /etc/lynx/lynx.lss

If you really want the terminal's default colors, and if lynx is built using
ncurses' default-color support, remove these two lines:

    #normal:   normal:   lightgray:black
    #default:  normal:   white:black

#### /etc/lynx/lynx.cfg

What I did to allow all cookies is open lynx.cfg in vim edit and change the following lines:

a)

    #FORCE_SSL_COOKIES_SECURE:FALSE

with

    FORCE_SSL_COOKIES_SECURE:TRUE

b)

    #SET_COOKIES:TRUE

uncomment it to:

    SET_COOKIES:TRUE

c) next, change

    ACCEPT_ALL_COOKIES:FALSE
    ACCEPT_ALL_COOKIES:TRUE

Onwards opening any website with lynx auto-accepts the cookies.


#### symlink config files

    ln -s ~/.config/lynx/lynx.cfg ~/.lynx.cfg
    ln -s ~/.config/lynx/lynx.lss ~/.lynx.lss

lrwxrwxrwx 1 mike mike 21 Jun 17 09:31 .lynx.cfg -> .config/lynx/lynx.cfg
lrwxrwxrwx 1 mike mike 21 Jun 17 09:31 .lynx.lss -> .config/lynx/lynx.lss
