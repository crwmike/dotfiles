" vim: filetype=vim :
"-------------------------------------------------------------------------------
" BASIC CONFIG: basic-confg.vim
"-------------------------------------------------------------------------------

" THEME:
        "colorscheme catppuccin_frappe
        colorscheme catppuccin_mocha
        "colorscheme catppuccin_macchiato

" HYBRID LINE NUMBERS:
        " Relative line numbers (absolute line number on current line)
        " Automatic toggle between line number modes (absolute in insert mode
        " and when the buffer doesn't have focus)
        set number relativenumber
        augroup numbertoggle
            autocmd!
            autocmd BufEnter,FocusGained,InsertLeave * set relativenumber
            autocmd BufLeave,FocusLost,InsertEnter   * set norelativenumber
        augroup END

" SOME BASIC SETTINGS:
        filetype plugin on                      " run plugin for filetype
        set title                               " filename in title
        set hidden                              " do not unload buffer when changing buffer
        set ignorecase                          " search case insensitive
        set smartcase                           " search case sensitive if any upercase letters
        set showcmd                             " show commands in lower right
        set cursorline                          " highlight cursor line
        "hi CursorLine   guibg=#20202f
        hi CursorLine   guibg=#222538
        hi CursorLineNr guifg=#ffffff   guibg=#3E4452
        hi CursorColumn guifg=#ffffff   guibg=#303446
        hi LineNr       guifg=#787894
        hi Comment      guifg=#9898b4   gui=italic
        hi EndOfBuffer  guifg=#6272a4
        " for markdown
        hi markdownBold         guifg=#eed49f
        hi markdownItalic       guifg=#56b6c2
        hi markdownCode         guifg=#a6da95
        hi markdownCodeBlock    guifg=#a6da95
        hi markdownBlockquote   guifg=#f5bde6
        set showbreak=↳                         " show char on wrapped lines
        set scrolloff=50                        " number of linesabove/below cursor line
        set hlsearch                            " Highlight all search results
        set splitbelow                          " :split opens new buffer below
        set splitright                          " :vsplit opens new buffer to the right
        set undofile                            " Maintain undo history between sessions
        set undodir=~/.confg/nvim/undo          " location of undo files
        set mouse=nv                            " enable mouse (a=all n=normal i=insert v=visual)
        set omnifunc=syntaxcomplete#Complete    " smart autocompletion (C-N, auto C-X C-O)
        set clipboard=unnamedplus               " Use system clipboard for yank/paste commands
        set foldlevel=20                        " disable folding on file open
        set textwidth=0 wrapmargin=0            " disable inserting LF on long lines
        set formatoptions=l                     " long lines are not broken in insert mode
        set nowrap linebreak nolist             " wrap long lines at word, off default
        set visualbell                          " Use visual bell (no beeping)
        set fillchars=vert:┃                    " vsplit char - below, vsplit color
        hi  VertSplit   guifg=#6272a4   ctermfg=61
        "hi  PmenuSel    guifg=#080808   guibg=#bd93f9   ctermfg=255     ctermbg=61
        hi  PmenuSel    guifg=#080808   guibg=#82aaff   ctermfg=255     ctermbg=61
        hi  Pmenu       guifg=#f8f8fa   guibg=#44475a   ctermfg=253     ctermbg=238
        hi  String      guifg=#afffaf                   ctermfg=253     ctermbg=238
        hi  SpecialKey  guifg=#61afef
        set laststatus=2                        " Always show statusline
        set termguicolors                       " TUI true color support
        set noshowmode                          " disable showmode; i.e. -- INSERT --
        set list                                " show nonprintable characters

" MOVEMENT IN INSERT MODE:
        inoremap <M-k> <up>
        inoremap <M-j> <down>
        inoremap <M-h> <left>
        inoremap <M-l> <right>

" AUTOINDENT SETTINGS:
        set tabstop=4       " number of columns for tabs
        set expandtab       " convert tabs to spaces
        set shiftwidth=4    " indent with reindent operation
        set softtabstop=4   " sets the number of columns for a TAB
        set autoindent      " indent next line to current
        set smartindent     " syntax indent gui

" FORCE SYNTAX:
        autocmd BufRead,BufNewFile *.ms,*.me,*.mom,*.man set filetype=groff
        autocmd BufRead,BufNewFile *.tex set filetype=tex
        autocmd BufRead,BufNewFile *.md set filetype=markdown
        autocmd BufRead,BufNewFile *.conf,*.zsh-theme setf conf
        autocmd BufRead,BufNewFile config setf conf
        autocmd BufRead,BufNewFile *.nix setf toml
        autocmd BufRead,BufNewFile *.rasi setf css
        autocmd BufRead,BufNewFile *.nvim setf vim
        autocmd BufRead,BufNewFile *.vifm setf vim
        "autocmd WinEnter,FileType markdown colorscheme palenight

" SWAP CAPSLOCK ESC:
        au VimEnter * silent! !xmodmap -e 'clear Lock' -e 'keycode 0x42 = Escape'
        "au VimLeave * silent! !xmodmap -e 'clear Lock' -e 'keycode 0x42 = Caps_Lock'

" BACKUP UNDO SWAP: files in directory:
        set backup
        set backupdir=~/.config/nvim/backup//
        set undodir=~/.config/nvim/undo//
        set directory=~/.config/nvim/swp//
        " Double slash does not actually work for backupdir, here's a fix...
        au BufWritePre * let &backupext='@'.substitute(substitute(substitute(expand('%:p:h'), '/', '%', 'g'), '\', '%', 'g'), ':', '', 'g')

" VERTICALLY CENTER DOCUMENT: (when entering insert mode)
        "autocmd InsertEnter * norm zz

" FINDING FILES:
        " Search down into subfolders, provide tab-completion for all file-related tasks
        " Display all matching files when we tab complete
        hi  IncSearch       guifg=#282c34   guibg=#e5c07b   gui=none
        hi  Search          guifg=#282c34   guibg=#61afef   gui=none
        set path+=**
        "set wildmenu
        "set wildmode=list:full
        "hi  WildMenu    ctermbg=141     ctermfg=0       guifg=#000000   guibg=#82aaff
        "hi  StatusLine  ctermbg=238     ctermfg=254     guifg=#8be9fd   guibg=#363848  gui=none
        set wildoptions=pum
        " Hit tab to :find by partial match
        " Use * to make it fuzzy

" NEOVIDE:
        if exists("g:neovide")
            " Put anything you want to happen only in Neovide here
            set guifont=Source\ Code\ Pro:h8
        endif


" HIGHLIGHT: when 81st column is reached
        highlight ColorColumn guibg=#20202f guifg=#ff5f87 ctermbg=none ctermfg=204
        set colorcolumn=81

" Make sure Vim returns to the same line when you reopen a file.
        augroup line_return
            au!
            au BufReadPost *
                \ if line("'\"") > 0 && line("'\"") <= line("$") |
                \     execute 'normal! g`"zvzz' |
                \ endif
        augroup END
" Thanks, Amit
