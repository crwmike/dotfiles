"  _       _ _         _
" (_)_ __ (_) |___   _(_)_ __ ___
" | | '_ \| | __\ \ / / | '_ ` _ \
" | | | | | | |_ \ V /| | | | | | |
" |_|_| |_|_|\__(_)_/ |_|_| |_| |_|
"
"===============================================================================
" VIM PLUG: --- plugin manager
"===============================================================================
if empty(glob("~/.config/nvim/autoload/plug.vim"))
    execute sh -c 'curl -fLo "${XDG_DATA_HOME:-$HOME/.local/share}"/nvim/site/autoload/plug.vim --create-dirs \
        https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
endif

call plug#begin('~/.config/nvim/plugged')
Plug 'mhinz/vim-startify'                                   " fancy start screen for Vim
Plug 'tpope/vim-fugitive'                                   " a git wrapper
Plug 'vim-scripts/AutoComplPop'                             " show Vim's complete menu while typing
Plug 'dhruvasagar/vim-table-mode'                           " automatic table creator
Plug 'cespare/vim-toml'                                     " Vim syntax for TOML.
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' } " fzf install for vim
Plug 'junegunn/fzf.vim'                                     " fzf for vim
Plug 'voldikss/vim-floaterm'                                " Use neovim terminal in the floating window.
Plug 'junegunn/goyo.vim'                                    " Distraction-free writing in Vim
Plug 'itchyny/lightline.vim'                                " A light statusline/tabline plugin
Plug 'mcchrish/nnn.vim'                                     " nnn file manager plugin
Plug 'francoiscabrol/ranger.vim'                            " ranger file manager plugin
Plug 'rbgrouleff/bclose.vim'                                " Delete a buffer without closing the window
Plug 'catppuccin/vim', { 'as': 'catppuccin' }
Plug 'dracula/vim',{'name':'dracula'}
Plug 'drewtempelmeyer/palenight.vim'
Plug 'lambdalisue/suda.vim'                                 " sudo save
Plug 'liuchengxu/vim-which-key'                             " which key on space
Plug 'liuchengxu/vim-which-key', { 'on': ['WhichKey', 'WhichKey!'] } " Display keybindings
Plug 'tpope/vim-surround'                                   " change surounding quote/brackets.
Plug 'yuezk/vim-js'                                         " syntax highlighting plugin for JavaScript
Plug 'tpope/vim-eunuch'                                     " Vim sugar for the UNIX shell commands that need it the most.
" ===  Any valid git URL is allowed  ===
Plug 'https://github.com/gorodinskiy/vim-coloresque.git'    " css/less/sass/html color preview
call plug#end()
"===============================================================================
" PUT YOUR NON PLUG STUFF AFTER THIS LINE:
"===============================================================================

" SOURCE Config Files: [ space+f+p ] to open config files
        source ~/.config/nvim/basic-config.vim
        source ~/.config/nvim/key-maps.vim
        source ~/.config/nvim/leader-key.vim
        source ~/.config/nvim/plug-config.vim

