" vim: filetype=vim :
"-------------------------------------------------------------------------------
" KEY MAPS: key-maps.vim
"-------------------------------------------------------------------------------

" BASIC KEY MAPPINGS:
        nnoremap <c-s> :update<CR>|             " Ctrl-S save file
        inoremap <c-s> <Esc>:update<CR>i|       " Ctrl-S save file, insert mode
        nnoremap <c-b> :Buffers<CR>|            " buffer menu
        nnoremap qq :w<CR>:bd<CR>|              " qq saves and closes the current buffer
        nnoremap <C-q> :qa<CR>|                 " quit all buffers
        nnoremap ; :|                           " That <SHIFT> is plain annoying
        nnoremap . ;|                           " repeat command
        nnoremap q: <nop>|                      " disable history in normal mode
        nnoremap U <c-r>|                       " map U to redo
        " vim.keymap.set("n", "U", "<C-r>")     " lua command format
        " Alternatives to <ESC>
        "inoremap jj <ESC>`^
        inoremap jk <ESC>`^
        inoremap kj <ESC>`^
        inoremap <C-c> <ESC>
        " show tabstops
        exec "set listchars=tab:\uBB\uB7,trail:\uA4,nbsp:~"
        " READMEs autowrap text
        autocmd BufRead,BufNewFile *.md set tw=79

" VERTICAL MOVEMENT WITH WRAPPED LINES:
        nnoremap j gj
        nnoremap k gk
        vnoremap j gj
        vnoremap k gk
        inoremap <Down> <C-o>gj
        inoremap <Up> <C-o>gk

" IN INSERT OR COMMAND MODE MOVE NORMALLY BY USING CTRL:
        inoremap <M-h> <Left>
        inoremap <M-j> <Down>
        inoremap <M-k> <Up>
        inoremap <M-l> <Right>
        cnoremap <M-h> <Left>
        cnoremap <M-j> <Down>
        cnoremap <M-k> <Up>
        cnoremap <M-l> <Right>

" THEME AND RELOAD KEYS: -- ~/.config/nvim/colors
        map <F1> :colorscheme dracula<CR>
        map <F2> :colorscheme nord<CR>
        map <F3> :so ~/.config/nvim/init.vim<CR>|   " relaod nvim config

" AUTOMATICALLY INSERT CLOSING BRACES WHEN TYPING OPENING BRACE:
        autocmd FileType html inoremap < <><left>
        inoremap { {}<left>
        inoremap ( ()<left>
        inoremap [ []<left>
        inoremap {<Space> {<Space><Space>}<left><left>
        inoremap (<Space> (<Space><Space>)<left><left>
        inoremap [<Space> [<Space><Space>]<left><left>
        inoremap [[ [[<Space><Space>]]<left><left><left>
        inoremap " ""<left>
        inoremap ' ''<left>
        autocmd FileType vim inoremap " "
        autocmd FileType markdown inoremap ' '
        autocmd FileType text inoremap ' '
        autocmd FileType lisp inoremap ' '
        autocmd FileType c inoremap < <><left>
        autocmd FileType cpp inoremap < <><left>
        autocmd FileType html inoremap < <><left>
        autocmd FileType xhtml  inoremap < <><left>
        autocmd FileType xml inoremap < <><left>
        inoremap (<CR> (<CR>)<ESC>O
        inoremap [<CR> [<CR>]<ESC>O
        inoremap {<CR> {<CR>}<ESC>O
        inoremap {;<CR> {<CR>};<ESC>O

" TAB NAVIGATION:
        noremap <M-up> :tabfirst<cr>
        noremap <M-down> :tablast<cr>
        noremap <M-left> :tabp<cr>
        noremap <M-right> :tabn<cr>

" BUFFER NAVIGATION:
        noremap <Tab> :up<CR>:bnext<CR>
        noremap <S-Tab> :up<CR>:bprevious<CR>
        noremap <C-n> :up<CR>:bnext<CR>
        noremap <C-p> :up<CR>:bprevious<CR>

" SPLIT NAVIGATION:
        nnoremap <M-Left> <C-w>h
        nnoremap <M-Down> <C-w>j
        nnoremap <M-Up> <C-w>k
        nnoremap <M-Right> <C-w>l

