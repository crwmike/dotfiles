" vim: filetype=vim :
"-------------------------------------------------------------------------------
" LEADER KEY AND WHICH KEY: leader-key.vim
"-------------------------------------------------------------------------------

" LEADER KEYS: (Space Key)
        let mapleader="\<Space>"
        nnoremap <leader><CR> :Startify<CR>|            " open Startify
        nnoremap <leader><Space> :nohlsearch<CR>|       " clear search highlights
        " vim-which-key
        nnoremap <silent> <leader> :WhichKey '<Space>'<CR>
        nnoremap <silent> <leader> :<c-u>WhichKey '<Space>'<CR>
        vnoremap <silent> <leader> :<c-u>WhichKeyVisual '<Space>'<CR>
        " insert menu [i]
        nnoremap <leader>ir :.!ruler<CR>|               " draw ruler in the current buffer
        nnoremap <leader>ib :.!shebang.sh --bash<CR>|   " insert a bash shebang in the current buffer
        nnoremap <leader>ih :.!script-head.sh --add<CR>|" insert header text in the current buffer
        nnoremap <leader>ip :.!shebang.sh --python<CR>| " insert a python shebang in the current buffer
        nnoremap <leader>iP :.!shebang.sh --perl<CR>|   " insert a perl shebang in the current buffer
        " misc menu [m]
        nnoremap <leader>mm :exec &mouse!=""? "set mouse=" : "set mouse=nv"<CR>| " toggle mouse
        nnoremap <leader>mu viwU<ESC>|                  " upper case word
        " run menu [r]
        nnoremap <leader>ro :.!|                        " pipe command output in current buffer
        nnoremap <leader>rl :.!ruler<CR>|               " draw ruler in the current buffer
        nnoremap <leader>rr :!|                         " run command
        " toggle menu [t]
        nnoremap <leader>tn :set number!<CR>:set relativenumber!<CR>
        " window menu [w]
        nnoremap <silent> <Leader>w+ :exe "resize " . (winheight(0) * 3/2)<CR>| " split larger
        nnoremap <silent> <Leader>w- :exe "resize " . (winheight(0) * 2/3)<CR>| " split smaller

" VIM WHICH KEY:
autocmd! User vim-which-key call which_key#register('<Space>', 'g:which_key_map')
let g:which_key_use_floating_win = 0
let g:which_key_vertical = 0
autocmd! FileType which_key
autocmd  FileType which_key set laststatus=0 noshowmode noruler
            \| autocmd BufLeave <buffer> set laststatus=2 showmode ruler
let g:which_key_map = {}
let g:which_key_map.b = {
            \ 'name' : '+Buffers',
            \ 'b' : [':Buffers'        , 'buffer menu (fzf)'],
            \ 'd' : [':bd!'            , 'close current buffer'],
            \ 's' : [':update'         , 'save buffer'],
            \ 't' : [':FloatermNew'    , 'open terminal in a floating window (floaterm)'],
            \ 'p' : [':bprev!'         , 'previous buffer'],
            \ 'n' : [':bnext!'         , 'next buffer']
            \ }
let g:which_key_map.c = {
            \ 'name' : '+Colorschemes',
            \ 'c' : [':Colors'         , 'select colorscheme (fzf)'],
            \ 'e' : [':Files ~/.config/nvim/colors/', 'open nvim colorscheme config file directory']
            \ }
let g:which_key_map.f = {
            \ 'name' : '+File',
            \ 'b' : [':Files ~/bin'    , 'open file in ~/bin'],
            \ 'c' : [':NnnPicker ~/.config', 'open file in .config'],
            \ 'f' : [':NnnPicker .'    , 'open with nnn in current directory'],
            \ 'h' : [':NnnPicker $HOME', 'open with nnn in <HOME> directory'],
            \ 'l' : [':NnnPicker ~/.local/bin'    , 'open file in ~/.local/bin'],
            \ 'n' : [':Explore .'      , 'open with netrw in current directory'],
            \ 'r' : [':History'        , 'open recent file (fzf)'],
            \ 'p' : [':NnnPicker ~/.config/nvim/', 'open nvim config files'],
            \ 's' : [':w'              , 'save file'],
            \ 'S' : [':SudaWrite'      , 'sudo and save current buffer'],
            \ 't' : [':NnnPicker .'    , 'open tree view in current directory'],
            \ 'v' : [':RangerNewTab'   , 'open file with ranger'],
            \ 'z' : [':Files .'        , 'fuzzy file finder (fzf)']
            \ }
let g:which_key_map.g = {
            \ 'name' : '+Git',
            \ 'a' : [':Git add .'      , 'stage all changed files'],
            \ 'c' : [':Git commit'     , 'commit staged files'],
            \ 'd' : [':Git diff .'     , 'show changes between commits (git-diff)'],
            \ 'f' : [':GitFiles'       , 'open git files (fzf)'],
            \ 'l' : [':Gclog'          , 'load commit history'],
            \ 'p' : [':Git push'       , 'push commit to gitlab.com'],
            \ 's' : [':Git status'     , 'git status']
            \ }
let g:which_key_map.h = {
            \ 'name' : '+Help',
            \ 'c' : [':Commands'       , 'search and run commands'],
            \ 'd' : [':h digraph-table', 'digraph table (unicode characters)'],
            \ 'h' : [':Helptags'       , 'search help tags'],
            \ 'm' : [':help'           , 'main help page'],
            \ 's' : [':setlocal spell! spelllang=en_us' , 'spell-check (en_us)']
            \ }
let g:which_key_map.i = {
            \ 'name' : '+Insert',
            \ 'b' :                      'insert a bash shebang in the current buffer',
            \ 'h' :                      'insert header text in the current buffer',
            \ 'p' :                      'insert a python shebang in the current buffer',
            \ 'P' :                      'insert a perl shebang in the current buffer',
            \ 'r' :                      'draw ruler in current buffer'
            \ }
let g:which_key_map.m = {
            \ 'name' : '+Misc',
            \ 'c' : [':setlocal spell! spelllang=en_us' , 'spell-check (en_us)'],
            \ 'g' : ['gg=G'            , 'cleanup code tabbing (gg=G)'],
            \ 'm' :                      'toggle mouse',
            \ 'r' : [':so ~/.config/nvim/init.vim' , 'reload nvim config'],
            \ 's' : [':%s/\s\+$//e'    , 'remove trailing spaces'],
            \ 't' : [':retab'          , 'convert tabs to spaces'],
            \ 'u' :                      'change word under cursor to upper case'
            \ }
let g:which_key_map.q = {
            \ 'name' : '+Quit/Save',
            \ 'w' : [':wa'             , '(:wa)  save all buffers, but do not exit'],
            \ 'q' : [':qa!'            , '(:qa!) quit without saving'],
            \ 's' : [':up'             , '(:up)  save only if buffer has been modified'],
            \ 'S' : [':SudaWrite'      , '(:SudaWrite) sudo and save current buffer'],
            \ 'x' : [':wqa'            , '(:wqa) save and quit all buffers'],
            \ 'b' : [':bd'             , '(:bd)  delete current buffer']
            \ }
let g:which_key_map.r = {
            \ 'name' : '+Run',
            \ 'o' :                      'pipe comand output into current buffer',
            \ 'f' : [':FloatermNew'    , 'open terminal in a floating window (floaterm)'],
            \ 'l' :                      'draw ruler in current buffer',
            \ 'r' :                      'run a command'
            \ }
let g:which_key_map.s = {
            \ 'name' : '+Search',
            \ 'a' : [':Ag'             , 'search for string in path (Ag, fzf)'],
            \ 'b' : [':BLines'         , 'search lines in current buffer (fzf)'],
            \ 'c' : [':History:'       , 'search (:) command history (fzf)'],
            \ 'f' : [':Files'          , 'open fuzzy find (fzf)'],
            \ 'h' : [':Helptags'       , 'search help tags (fzf)'],
            \ 'l' : [':Lines'          , 'search lines in buffers (fzf)'],
            \ 'r' : [':Rg'             , 'search for string in path (rg, fzf)'],
            \ 's' : [':History/'       , 'search (/) search history (fzf)']
            \ }
let g:which_key_map.t = {
            \ 'name' : '+Toggle',
            \ 'c' : [':set cursorcolumn!', 'toggle CursorColumn'],
            \ 'g' : [':Goyo'           , 'toggle Goyo (distraction free mode)'],
            \ 'm' :                      'toggle table mode (markdown)',
            \ 'l' : [':set list!'      , 'toggle show white spaces'],
            \ 'n' :                      'toggle line numbers',
            \ 'r' : [':set relativenumber!' , 'toggle relative line numbers'],
            \ 's' : [':setlocal spell! spelllang=en_us' , 'toggle spell-check (en_us)'],
            \ 't' : [':NnnPicker .'    , 'open tree view in current directory'],
            \ 'w' : [':set wrap!'      , 'toggle word wrap']
            \ }
let g:which_key_map.w = {
            \ 'name' : '+Window',
            \ '+' :                      'resize window larger',
            \ '-' :                      'resize window smaller',
            \ '=' : ['<C-w>='          , 'resize windows equal'],
            \ 'w' : ['<C-w>w'          , 'cycle through windows'],
            \ 'h' : ['<C-w>h'          , 'switch to window left'],
            \ 'j' : ['<C-w>j'          , 'switch to window down'],
            \ 'k' : ['<C-w>k'          , 'switch to window up'],
            \ 'l' : ['<C-w>l'          , 'switch to window right'],
            \ 's' : [':split'          , 'split horizontal'],
            \ 'v' : [':vsplit'         , 'split vertical'],
            \ 'm' : [':Windows'        , 'window menu (fzf)']
            \ }
let g:which_key_map.z = {
            \ 'name' : '+Folds',
            \ 'a' : ['za'            , '(za) toggle fold'],
            \ 'A' : ['zA'            , '(zA) toggle all folds'],
            \ 'c' : ['zc'            , '(zc) close one fold'],
            \ 'C' : ['zC'            , '(zC) close all folds'],
            \ 'd' : ['zd'            , '(zd) delete a fold'],
            \ 'D' : ['zD'            , '(zD) delete folds recursively'],
            \ 'E' : ['zE'            , '(zE) eliminate all folds'],
            \ 'f' : ['zf'            , '(zf) create fold of selection'],
            \ 'o' : ['zo'            , '(zo) open one folds'],
            \ 'O' : ['zO'            , '(zO) open all folds'],
            \ 'v' : ['zv'            , '(zv) view cursor line']
            \ }

