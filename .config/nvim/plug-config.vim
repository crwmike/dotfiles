" vim: filetype=vim :
"-------------------------------------------------------------------------------
" PLUG CONFIG: plug-config.vim
"-------------------------------------------------------------------------------

" BUFTABLINE:
        " https://github.com/ap/vim-buftabline
        let g:buftabline_show = 1
        let g:buftabline_numbers = 1
        let g:buftabline_indicators = 1
        let g:buftabline_separators = 1
        " buffer tab colors:
        hi  TabLineFill  guifg=#6272a4  guibg=#1e1e2e
        hi  TabLine      guifg=#b8c0e0  guibg=#303446
        hi  TabLineSel   guifg=#000000  guibg=#82aaff

" FLOATERM:
        let g:floaterm_width=0.9
        let g:floaterm_height=0.6
        hi Floaterm guibg=#202130
        hi FloatermBorder guibg=#202130 guifg=#bd93f9

" FZF COLORS:
        " Customize fzf colors to match your color scheme
        let g:fzf_colors =
            \ { 'fg':    ['fg', 'Normal'],
            \ 'bg':      ['bg', 'Normal'],
            \ 'hl':      ['fg', 'Number'],
            \ 'fg+':     ['fg', 'CursorLine', 'CursorColumn', 'Normal'],
            \ 'bg+':     ['bg', 'CursorLine', 'CursorColumn'],
            \ 'hl+':     ['fg', 'Statement'],
            \ 'info':    ['fg', 'PreProc'],
            \ 'border':  ['fg', 'Comment'],
            \ 'prompt':  ['fg', 'Conditional'],
            \ 'pointer': ['fg', 'Exception'],
            \ 'marker':  ['fg', 'Keyword'],
            \ 'spinner': ['fg', 'Label'],
            \ 'header':  ['fg', 'Comment'] }

" GOYO: - https://github.com/junegunn/goyo.vim
        " ENTER
        function! s:goyo_enter()
            set noshowmode
            set noshowcmd
            set norelativenumber
            set nonumber
            set wrap
            set nocursorline
        endfunction
        " LEAVE
        function! s:goyo_leave()
            set noshowmode
            set showcmd
            set number
            set nowrap
            set relativenumber
            set cursorline
        endfunction
        " call functions
        autocmd! User GoyoEnter nested call <SID>goyo_enter()
        autocmd! User GoyoLeave nested call <SID>goyo_leave()

" ILLUMINATE: - https://github.com/RRethy/vim-illuminate
        " Time in milliseconds (default 250)
        let g:Illuminate_delay = 150
        " Don't highlight word under cursor (default: 1)
        let g:Illuminate_highlightUnderCursor = 1
        hi illuminatedWord ctermbg=238 guibg=#44475a

" LIGHTLINE: - The colorscheme for lightline.vim.
        " CURRENTLY: wombat, solarized, powerline, powerlineish, jellybeans,
        " molokai, seoul256, darcula, selenized_dark, selenized_black,
        " selenized_light, selenized_white, Tomorrow, Tomorrow_Night,
        " Tomorrow_Night_Blue, Tomorrow_Night_Bright, Tomorrow_Night_Eighties,
        " PaperColor, landscape, one, materia, material, OldHope, nord, deus,
        " simpleblack, srcery_drk, ayu_mirage, ayu_light, ayu_dark.
        " ----------------------------------------------------------------------
        " wombat, *OldHope, darcula, deus, *material, one, rosepine
        let g:lightline = {
            \ 'colorscheme': 'material',
            \ 'active': {
            \   'left': [ [ 'mode', 'paste' ],
            \             [ 'gitbranch', 'readonly', 'filename', 'modified' ] ]
            \ },
            \ 'mode_map': {
            \ 'n' : 'NOR',
            \ 'i' : 'INSERT'
            \},
            \ 'component_function': {
            \   'gitbranch': 'FugitiveHead'
            \ },
            \ 'tabline': {
            \   'left': [['buffers']],
            \   'right': [['close']]
            \ },
            \ 'component_expand': {
            \   'buffers': 'lightline#bufferline#buffers'
            \ },
            \ 'component_type': {
            \   'buffers': 'tabsel'
            \ },
            \ }

" VIM MARKDOWN:
        " disable the folding configuration:
        let g:vim_markdown_folding_disabled = 1

" NETRW:
        let g:netrw_banner = 1
        let g:netrw_liststyle = 1
        let g:netrw_browse_split = 0

" NNN:
        let g:nnn#layout = { 'left': '~25%' }
        "let g:nnn#layout = 'new'
        "let g:nnn#layout = 'vnew'
        let g:nnn#command = 'nnn'
        let g:nnn#statusline = 0
        let g:nnn#replace_netrw = 1
        "
" PALENIGHT:
        let g:palenight_terminal_italics=1

" PYTHON:
        let g:python_host_program = '/usr/bin/python'
        let g:python3_host_program = '/usr/bin/python3'

" RANGER:
        let g:ranger_map_keys = 0

" TOKYONIGHT:
        let g:tokyonight_style = 'storm' " available: night, storm
        let g:tokyonight_enable_italic = 1

" SQL:
        " Default to static completion for SQL
        let g:omni_sql_default_compl_type = 'syntax'


" STARTIFY: - https://github.com/mhinz/vim-startify
        hi StartifyHeader guifg=#8bd5ca
        hi StartifyFooter guifg=#8bd5ca
        hi StartifyFile guifg=#ffffff
        hi StartifySpecial guifg=#ed8796
        hi StartifyNumber guifg=#eed49f
        hi StartifyBracket guifg=#b8c0e0
        hi StartifyPath guifg=#8aadf4
        hi StartifySlash guifg=#8aadf4
        hi StartifySection guifg=#f5bde6
        let g:startify_custom_header = [
            \ '           ███▄    █ ▓█████  ▒█████   ██▒   █▓ ██▓ ███▄ ▄███▓',
            \ '           ██ ▀█   █ ▓█   ▀ ▒██▒  ██▒▓██░   █▒▓██▒▓██▒▀█▀ ██▒',
            \ '          ▓██  ▀█ ██▒▒███   ▒██░  ██▒ ▓██  █▒░▒██▒▓██    ▓██░',
            \ '          ▓██▒  ▐▌██▒▒▓█  ▄ ▒██   ██░  ▒██ █░░░██░▒██    ▒██ ',
            \ '          ▒██░   ▓██░░▒████▒░ ████▓▒░   ▒▀█░  ░██░▒██▒   ░██▒',
            \ '          ░ ▒░   ▒ ▒ ░░ ▒░ ░░ ▒░▒░▒░    ░ ▐░  ░▓  ░ ▒░   ░  ░',
            \ '          ░ ░░   ░ ▒░ ░ ░  ░  ░ ▒ ▒░    ░ ░░   ▒ ░░  ░      ░',
            \ '             ░   ░ ░    ░   ░ ░ ░ ▒       ░░   ▒ ░░      ░   ',
            \ '                   ░    ░  ░    ░ ░        ░   ░         ░   ',
            \ '                                          ░                  ',
            \ ]
        let g:startify_custom_footer = [
            \ '',
            \ '     Neovim: hyperextensible Vim-based text editor   ',
            \ ]
        let g:startify_padding_left = 5
        let g:startify_lists = [
            \ { 'type': 'files',     'header': ['   Recent Files']      },
            \ { 'type': 'bookmarks', 'header': ['   Bookmarks']         },
            \ { 'type': 'dir',       'header': ['   PWD '. getcwd()]    },
            \ ]
        let g:startify_bookmarks = [
            \ { 'a': '~/.aliases'                                       },
            \ { 'b': '~/.config/nvim/basic-config.vim'                  },
            \ { 'c': '~/.config/qtile/config.py'                        },
            \ { 'l': '~/.config/nvim/leader-key.vim'                    },
            \ { 'm': '~/.config/nvim/key-maps.vim'                      },
            \ { 'n': '~/.config/nvim/init.vim'                          },
            \ { 'p': '~/.config/nvim/plug-config.vim'                   },
            \ { 'r': '~/.reminders'                                     },
            \ { 's': '~/.config/starship.toml'                          },
            \ { 'S': '~/.config/qtile/scripts/autostart.sh'             },
            \ { 'x': '~/.config/qtile/sxhkd/sxhkdrc'                    },
            \ { 't': '~/.config/alacritty/alacritty.toml'               },
            \ { 'T': '~/.config/kitty/kitty.conf'                       },
            \ { 'w': '~/.config/wezterm/wezterm.lua'                    },
            \ { 'y': '~/.config/yazi/yazi.toml'                         },
            \ { 'z': '~/.zshrc'                                         },
            \ ]

