" vim: filetype=vim :
"        _
" __   _(_) _____      ___ __ ___
" \ \ / / |/ _ \ \ /\ / / '__/ __|
"  \ V /| |  __/\ V  V /| | | (__
" (_)_/ |_|\___| \_/\_/ |_|  \___|
"
" .viewrc -- settings for NVIM invoked in read-only mode

" THEME:
        colorscheme tokyonight

" TOKYONIGHT:
        let g:tokyonight_style = 'storm' " available: night, storm
        let g:tokyonight_enable_italic = 1

" Some Basic Settings:
        syntax enable
        set termguicolors           " TUI true color support
        set nu                      " set line numbers
        set hlsearch                " highlight search results
        set ignorecase              " ignore case in pattern
        set incsearch               " show search result as typing
        set listchars+=precedes:<,extends:>
        set scroll=1                " number of lines scrolled C-u C-d
        set scrolloff=999           " center cursor
        set shortmess=filmnrxt      " avoid Hit Enter messages
        set showbreak=↳             " string at the start of wrapped line
        set smartcase               " case sensitive search if stringe has upper case
        set nostartofline           " move to first non blanc char in line
        set nowrap linebreak nolist   " soft word wrap
        "hi  NonText                 guifg=#bd93f9   guibg=#181926   gui=none

" KEYMAPS:
        nnoremap <Down> 1<C-D>
        nnoremap <Up> 1<C-U>
        nnoremap <Left> 4zh
        nnoremap <Right> 4zl
        nnoremap <Home> 1G
        nnoremap <End> G
        nnoremap <BS> <C-B>
        nnoremap <Space> <C-F>
        nnoremap <F2> :set invwrap<CR>
        nnoremap <F7> /\V
        nnoremap <Esc>u :nohl<CR>
        nnoremap q :q<CR>

" Buffer Navigation:
        nnoremap <Tab> :bnext<CR>
        nnoremap <S-Tab> :bprevious<CR>
        nnoremap <C-n> :bnext<CR>
        nnoremap <C-p> :bprevious<CR>

" VIM_BUFTABLINE:
        " https://github.com/ap/vim-buftabline
        let g:buftabline_show = 1
        let g:buftabline_numbers = 1
        let g:buftabline_indicators = 0
        let g:buftabline_separators = 1
        hi  TabLineFill     guifg=#8aadf4   guibg=#1e1e2e   gui=none
        hi  TabLine         guifg=#f5bde6   guibg=#1e1e2e   gui=none
        hi  TabLineSel      guifg=#ffffff   guibg=#1e1e2e   gui=bold

" STATUSLINE:
"       "   catppuccin
        "   bk=#20203a  rd=#ed8796  gr=#a6da95  yl=#eed49f,
        "   bl=#8aadf4  ma=#f5bde6  cy=#56b6c2  wh=#b8c0e0
        "   ========================================================
        hi  StatuslineName  guifg=#5fffd7   guibg=#1e1e2e   gui=none
        hi  StatuslineNC    guifg=#a6da95   guibg=#1e1e2e   gui=none
        hi  Statusline      guifg=#f5bde6   guibg=#1e1e2e   gui=none
        hi  ModeMsg         guifg=#8aadf4   guibg=#1e1e2e   gui=none
        hi  IncSearch       guifg=#1e1e2e   guibg=#eed49f   gui=none
        hi  Search          guifg=#000000   guibg=#5fffd7   gui=none
        hi  NonText         ctermfg=93      guifg=#f5bde6   gui=none
        hi  LineNr          ctermfg=141     guifg=#8aadf4   gui=none
        set laststatus=2
        set statusline=
        set statusline+=%#TabLineSel#[%n]\              " buffer num, file name
        set statusline+=%#StatuslineName#\ %F\          " buffer num, file name
        set statusline+=%#Statusline#\ %=\              " separator
        set statusline+=%#StatuslineNC#\ %y\            " filetype
        set statusline+=%#Statusline#\ %l/%L            " current/total lines
        set statusline+=%#ModeMsg#%5P\                  " percent file

