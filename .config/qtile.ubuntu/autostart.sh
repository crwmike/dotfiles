#!/bin/bash

function run {
  if ! pgrep $1 ;
  then
    $@&
  fi
}

#starting utility applications at boot time
#run variety &
#nitrogen --restore &
run udiskie -ans &
run dunst &
#run compton --config ~/.config/compton/compton.conf &
run dropbox start &
#run numlockx on &
#run nm-applet &
#run xfce4-power-manager --restart &
run /usr/bin/emacs --daemon &
#run touchpad-indicator &
#feh --randomize --bg-fill /home/mike/Pictures/Backgrounds/* &
#/usr/bin/brave-browser &
/usr/bin/vivaldi &
echo "[$(date)] 'autostart.sh': 'Qtile' started (login)" >> /home/mike/.local/log/cron.log
espeak "ready"
