# -*- coding: utf-8 -*-
#     ___      _    _   __
#   .'   `.   / |_ (_) [  |
#  /  .-.  \ `| |-'__   | | .---.
#  | |   | |  | | [  |  | |/ /__\\
#  \  `-'  \_ | |, | |  | || \__.,
#   `.___.\__|\__/[___][___]'.__.'
#
# Copyright (c) 2010 Aldo Cortesi
# Copyright (c) 2010, 2014 dequis
# Copyright (c) 2012 Randall Ma
# Copyright (c) 2012-2014 Tycho Andersen
# Copyright (c) 2012 Craig Barnes
# Copyright (c) 2013 horsik
# Copyright (c) 2013 Tao Sauvage
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

##### IMPORTS #####
import os
import re
import socket
import subprocess
from libqtile.config import Key, KeyChord, Match, Screen, Group, Drag, Click
from libqtile.command import lazy
from libqtile import layout, bar, widget, hook
from typing import List  # noqa: F401
from libqtile.config import ScratchPad, DropDown

##### DEFINING SOME VARIABLES #####
mod = "mod4"                                     # Sets mod key to SUPER/WINDOWS
myTerm = "alacritty"                             # My terminal of choice
myBrowser = 'vivaldi'                            # My browser of choice
myConfig = "/home/mike/.config/qtile/config.py"  # The Qtile config file location

##### KEYBINDINGS #####
keys = [
         ### The essentials
         Key([mod               ], "Return", lazy.spawn("alacritty"),                   desc='Launch alacritty'),
         Key(["control", "mod1" ], "Return", lazy.spawn("kitty"),                       desc='Launch kitty'),
         Key([mod, "shift"      ], "Return", lazy.spawn("pcmanfm"),                     desc='PCManFM File Manager'),
         Key([mod, "control"    ], "Return", lazy.spawn("nemo"),                        desc='Nemo File Manager'),
         Key([mod, "mod1"       ], "Return", lazy.spawn("gksudo pcmanfm"),              desc='Launch File Manager as root'),
         Key(["mod1"            ], "Tab",    lazy.next_layout(),                        desc='Toggle through layouts'),
         Key([mod, "shift"      ], "c",      lazy.window.kill(),                        desc='Kill active window'),
         Key([mod, "shift"      ], "l",      lazy.spawn("i3lock --color 1e1e2e"),       desc='Lock the screen'),
         Key([mod, "control"    ], "r",      lazy.restart(),                            desc='Restart Qtile'),
         Key([mod, "control"    ], "q",      lazy.shutdown(),                           desc='Shutdown Qtile'),
         Key(["control", "shift"], "q",      lazy.window.kill(),                        desc='Kill active window'),

         ### dmenu and rofi
         Key([mod, "shift"      ], "d",      lazy.spawn("dmenu_run -i -nb '#1e1e2e' -nf '#56b6c2' -sb '#56b6c2' -sf '#000000' -fn 'Source Code Pro:pixelsize=15' -p 'dmenu_run: '"), desc='Dmenu Run Launcher'),
         Key([mod               ], "d",      lazy.spawn("dmenu-desktop"),               desc='Dmenu Desktop Launcher'),
         Key(["control", "shift"], "c",      lazy.spawn("rofi-calc"), desc='rofi calculator'),
         Key([mod               ], "space",  lazy.spawn("rofi -show drun -columns 2 -show-icons"), desc='rofi drun'),
         Key([mod, "mod1"       ], "space",  lazy.spawn("rofi -show run -columns 4"),   desc='rofi run'),
         Key([mod, "control"    ], "space",  lazy.spawn("rofi -show run -columns 3 -display-run ' SUDO RUN ' -run-command 'gksudo {cmd}'"),   desc='rofi run as root'),
         Key([mod               ], "Tab",    lazy.spawn("rofi -show window -columns 1"), desc='rofi window list'),
         Key(["control", "mod1" ], "Delete", lazy.spawn("dmpower"),                     desc='dmenu shutdown menu'),
         Key([mod, "shift"      ], "a",      lazy.spawn("dmsounds"),                    desc='ambient brackground sounds'),
         Key([mod, "shift"      ], "m",      lazy.spawn("mp"),                          desc='rofi man page menu'),
         Key([mod, "shift"      ], "p",      lazy.spawn("dmconf"),                      desc='dmenu config edit menu'),
         Key([mod, "shift"      ], "u",      lazy.spawn("utils"),                       desc='rofi cli utils menu'),
         Key([mod, "shift"      ], "w",      lazy.spawn("dm-wifi"),                     desc='dmenu wifi menu'),
         Key([mod               ], "x",      lazy.spawn("rofi-power exit"),             desc='rofi shutdown menu'),
         Key([mod, "shift"      ], "z",      lazy.spawn("rofi-edit"),                   desc='rofi config edit menu'),
         Key([mod               ], "z",      lazy.spawn("xfce4-appfinder -r"),          desc='Xfce appfinder'),

         ### Treetab controls
         Key([mod, "control"    ], "k",      lazy.layout.section_up(),                  desc='Move up a section in treetab'),
         Key([mod, "control"    ], "j",      lazy.layout.section_down(),                desc='Move down a section in treetab'),

         ### Window controls
         Key([mod               ], "h",      lazy.layout.left(),                        desc='Move focus down in current stack pane'),
         Key([mod               ], "j",      lazy.layout.down(),                        desc='Move focus down in current stack pane'),
         Key([mod               ], "k",      lazy.layout.up(),                          desc='Move focus up in current stack pane'),
         Key([mod               ], "l",      lazy.layout.right(),                       desc='Move focus down in current stack pane'),
         Key([mod               ], "Down",   lazy.layout.down(),                        desc='Move focus down in current stack pane'),
         Key([mod               ], "Up",     lazy.layout.up(),                          desc='Move focus up in current stack pane'),
         Key([mod, "shift"      ], "h",      lazy.layout.shuffle_left(),                desc='Move windows down in current stack'),
         Key([mod, "shift"      ], "j",      lazy.layout.shuffle_down(),                desc='Move windows down in current stack'),
         Key([mod, "shift"      ], "k",      lazy.layout.shuffle_up(),                  desc='Move windows up in current stack'),
         Key([mod, "shift"      ], "l",      lazy.layout.shuffle_right(),               desc='Move windows up in current stack'),
         Key([mod, "shift"      ], "Down",   lazy.layout.shuffle_down(),                desc='Move windows down in current stack'),
         Key([mod, "shift"      ], "Up",     lazy.layout.shuffle_up(),                  desc='Move windows up in current stack'),
         Key(["control", "shift"], "b",      lazy.hide_show_bar("top"),                 desc="toggle bar"),
         Key([mod, "control"    ], "l",      lazy.layout.grow(), lazy.layout.increase_nmaster(),    desc='Expand window (MonadTall), increase number in master pane (Tile)'),
         Key([mod, "control"    ], "h",      lazy.layout.shrink(), lazy.layout.decrease_nmaster(),  desc='Shrink window (MonadTall), decrease number in master pane (Tile)'),
         Key([mod               ], "n",      lazy.layout.normalize(),                   desc='normalize window size ratios'),
         Key([mod               ], "m",      lazy.layout.maximize(),                    desc='toggle window between minimum and maximum sizes'),
         Key([mod, "shift"      ], "f",      lazy.window.toggle_floating(),             desc='toggle floating'),
         Key([mod,              ], "f",      lazy.window.toggle_fullscreen(),           desc='toggle fullscreen'),

         ### Stack controls
         Key([mod, "shift"      ], "space",  lazy.layout.rotate(), lazy.layout.flip(),  desc='Switch which side main pane occupies (XmonadTall)'),
         Key([mod, "shift"      ], "h",      lazy.layout.next(),                        desc='Switch window focus to other pane(s) of stack'),
         Key([mod, "control"    ], "s",      lazy.layout.toggle_split(),                desc='Toggle between split and unsplit sides of stack'),

         ### My applications
         Key([mod               ], "a",      lazy.spawn("lxappearance"),                desc='Lxqt appearance settings'),
         Key([mod, "shift"      ], "b",      lazy.spawn("firefox"),                     desc='Firefox browser'),
         Key([mod, "control"    ], "b",      lazy.spawn("brave-browser"),               desc='Brave browser'),
         Key([mod               ], "b",      lazy.spawn("vivaldi-stable"),              desc='Vivaldi browser'),
         Key([mod, "mod1"       ], "c",      lazy.spawn("mate-color-select"),           desc='Color Picker'),
         Key([mod               ], "e",      lazy.spawn("emacsclient -c -a emacs"),   desc='emacs'),
         Key([mod               ], "g",      lazy.spawn("gimp"),                        desc='GIMP'),
         Key([mod, "shift"      ], "n",      lazy.spawn("wallpaper-changer"),           desc='Next wallpaper'),
         Key([mod               ], "p",      lazy.spawn("polarr"),                      desc='Polarr'),
         Key([mod               ], "r",      lazy.spawn("rhythmbox %U"),                desc='Rhythmbox'),
         Key([mod, "shift"      ], "s",      lazy.spawn("flameshot gui"),               desc='screenshot with Flameshot'),
         Key([mod, "mod1"       ], "t",      lazy.spawn("geany"),                       desc='Geany'),
         Key([mod               ], "t",      lazy.spawn("notepadqq"),                   desc='notepadqq editor'),
         Key([mod, "shift"      ], "t",      lazy.spawn("subl"),                        desc='Sublime Text'),
         Key([mod, "control"    ], "t",      lazy.spawn("pluma"),                       desc='Pluma editor'),
         Key([mod               ], "v",      lazy.spawn("virtualbox"),                  desc='VirtualBox'),
         Key([mod, "shift"      ], "v",      lazy.spawn("/usr/bin/flatpak run --branch=stable --arch=x86_64 --command=kdenlive --file-forwarding org.kde.kdenlive @@ %F @@"), desc='Kdenlive'),
         Key([mod, "shift"      ], "x",      lazy.spawn("dmkill"),                      desc='dmenu kill process'),
         Key([mod, "control"    ], "x",      lazy.spawn("/usr/bin/xkill"),              desc='xkill'),

         ### Hardware Keys
         Key([], "XF86AudioRaiseVolume",     lazy.spawn("amixer -D pulse sset Master 5%+"), desc='volume up'),
         Key([], "XF86AudioLowerVolume",     lazy.spawn("amixer -D pulse sset Master 5%-"), desc='volume down'),
         Key([], "Print",                    lazy.spawn('xfce4-screenshooter'),             desc='take a screenshot')
]


##### GROUPS #####
group_names = [("1", {'layout': 'monadwide'}),
               ("2", {'layout': 'monadtall'}),
               ("3", {'layout': 'monadtall'}),
               ("4", {'layout': 'monadtall'}),
               ("5", {'layout': 'monadtall'}),
               ("6", {'layout': 'monadtall'}),
               ("7", {'layout': 'monadtall'}),
               ("8", {'layout': 'monadtall'}),
               ("9", {'layout': 'max'})]

groups = [Group(name, **kwargs) for name, kwargs in group_names]

for i, (name, kwargs) in enumerate(group_names, 1):
    keys.append(Key([mod              ], "Right", lazy.screen.next_group()))     # Switch next group
    keys.append(Key(["control", "mod1"], "Right", lazy.screen.next_group()))     # Switch next group
    keys.append(Key([mod              ], "Left",  lazy.screen.prev_group()))     # Switch previous group
    keys.append(Key(["control", "mod1"], "Left",  lazy.screen.prev_group()))     # Switch previous group
    keys.append(Key([mod              ], str(i),  lazy.group[name].toscreen()))  # Switch to another group
    keys.append(Key([mod, "shift"     ], str(i),  lazy.window.togroup(name) , lazy.group[name].toscreen())) # Send current window to another group

###  SCRATCHPADS  ####################################################################################################
groups.append(
    ScratchPad("scratchpad", [
        DropDown("1", "kitty",                              x=0.25, y=0.25, width=0.5,      height=0.5,  opacity=1.0),
        DropDown("2", "pluma Documents/notepad.md",         x=0.25, y=0.25, width=0.5,      height=0.5,  opacity=1.0),
        DropDown("3", "kitty -e qkeys -h",                  x=0.25, y=0.1,  width=0.48,     height=0.70, opacity=1.0),
        DropDown("4", "speedcrunch",                        x=0.30, y=0.30, width=0.35,     height=0.4,  opacity=1.0)
    ])
)

keys.extend([
        Key(["control", "shift"], "a", lazy.group["scratchpad"].dropdown_toggle("1")),
        Key(["control", "shift"], "w", lazy.group["scratchpad"].dropdown_toggle("2")),
        Key([mod               ], "s", lazy.group["scratchpad"].dropdown_toggle("3")),
        Key([mod               ], "c", lazy.group["scratchpad"].dropdown_toggle("4"))
])
######################################################################################################################

##### DEFAULT THEME SETTINGS FOR LAYOUTS #####
layout_theme = {"border_width": 2,
                "margin": 10,
                "border_focus": "8aadf4",
                "border_normal": "303446",
                "new_at_current": "True"
                }

##### THE LAYOUTS #####
layouts = [
    layout.MonadTall(
        new_client_position = 'top',
        single_border_width = 0,
        single_margin = 0,
        **layout_theme
        ),
    layout.Max(**layout_theme),
    #layout.Bsp(**layout_theme),
    #layout.Stack(stacks=2, **layout_theme),
    #layout.Columns(**layout_theme),
    layout.RatioTile(
        ratio = 1.618,
        **layout_theme),
    #layout.VerticalTile(**layout_theme),
    #layout.Zoomy(**layout_theme),
    layout.MonadWide(
        new_client_position = 'top', 
        single_border_width = 0,
        single_margin = 0,
        **layout_theme
        ),
    #layout.Tile(
    #    shift_windows=False, 
    #    **layout_theme
    #    ),
    #layout.TreeTab(
    #     font = "Ubuntu",
    #     fontsize = 12,
    #     sections = ["Section 1", "Section 2"],
    #     section_fontsize = 12,
    #     bg_color = "181a26",
    #     active_bg = "bd93f9",
    #     active_fg = "000000",
    #     inactive_bg = "6272a4",
    #     inactive_fg = "f8f8f2",
    #     padding_y = 5,
    #     section_top = 12,
    #     panel_width = 150
    #     ),
    #layout.Matrix(**layout_theme),
    #layout.Floating(**layout_theme)
]

##### COLORS #####
colors = [["#1e1e2e"], # 0 panel background
          ["#8f8fa0"], # 1 gray (div)
          ["#e8efff"], # 2 white (font color for group names)
          ["#e1acff"], # 3 purple
          ["#9cc4ff"], # 4 blue
          ["#89ddff"], # 5 cyan
          ["#6dffd7"], # 6 green
          ["#ffe585"], # 7 orange
          ["#ffe585"], # 8 yellow
          ["#60607f"], # 9 gray (inactive)
          ["#24273a"]] # 10 dark gray

widget_defaults = dict(
    font='Source Code Pro',
    fontsize=16,
    padding=3,
)
extension_defaults = widget_defaults.copy()

##### WIDGETS #####
screens = [
    Screen(
        top=bar.Bar(
            [
                widget.Sep(
                        linewidth = 0,
                        padding = 6,
                        foreground = colors[2],
                        background = colors[0]
                        ),
                widget.GroupBox(font="Source Code Pro",
                        fontsize = 20,
                        margin_y = 3,
                        margin_x = 0,
                        padding_y = 1,
                        padding_x = 4,
                        borderwidth = 3,
                        active = colors[2],
                        inactive = colors[1],
                        rounded = True,
                        highlight_color = colors[1],
                        highlight_method = "block",
                        block_highlight_text_color = colors[0],
                        urgent_alert_method = "text",
                        urgent_text = colors[8],
                        this_current_screen_border = colors[4],
                        this_screen_border = colors [3],
                        other_current_screen_border = colors[0],
                        other_screen_border = colors[1],
                        foreground = colors[2],
                        background = colors[0]
                        ),
                widget.TextBox(
                        text='] ',
                        foreground = colors[0],
                        background = colors[0],
                        padding=0,
                        fontsize=12
                        ),
                widget.Prompt(),
                widget.Sep(
                        linewidth = 0,
                        padding = 4,
                        foreground = colors[0],
                        background = colors[0]
                        ),
                widget.TextBox(
                        text='| ',
                        background = colors[0],
                        foreground = colors[0],
                        padding=0,
                        fontsize=12
                        ),
                widget.WindowName(
                        font="Source Code Pro",
                        fontsize = 16,
                        foreground = colors[2],
                        background = colors[0],
                        padding = 5
                        ),
                widget.TextBox(
                        text=' ',
                        background = colors[0],
                        foreground = colors[0],
                        padding=0,
                        fontsize=12
                        ),
                widget.TextBox(
                        text='',
                        background = colors[0],
                        foreground = colors[0],
                        padding=0,
                        fontsize=34
                        ),
                widget.TextBox(
                        text='| ',
                        background = colors[0],
                        foreground = colors[0],
                        padding=0,
                        fontsize=12
                        ),
                widget.Systray(
                        background=colors[0],
                        padding = 5
                        ),
                widget.TextBox(
                        text=' |',
                        background = colors[0],
                        foreground = colors[1],
                        padding=0,
                        fontsize=12
                        ),
                widget.TextBox(
                        text=' 💻',
                        foreground = colors[3],
                        background = colors[0],
                        padding=0,
                        fontsize=12
                        ),
                widget.CurrentLayout(
                        foreground = colors[3],
                        background = colors[0],
                        padding = 4
                        ),
                widget.TextBox(
                        text='',
                        background = colors[0],
                        foreground = colors[0],
                        padding=0,
                        fontsize=16
                        ),
                widget.TextBox(
                        text = ' 🔊',
                        #text=" Vol:",
                        foreground = colors[4],
                        background = colors[0],
                        padding = 0,
                        fontsize=12
                        ),
                widget.Volume(
                        foreground = colors[4],
                        background = colors[0],
                        padding = 5
                        ),
                widget.TextBox(
                        text='',
                        background = colors[0],
                        foreground = colors[0],
                        padding=0,
                        fontsize=12
                        ),
                widget.TextBox(
                        text=" 🔋",
                        foreground = colors[5],
                        background = colors[0],
                        padding = 0,
                        fontsize=12
                        ),
                widget.Battery(
                        foreground=colors[5],
                        background=colors[0],
                        update_interval = 10,
                        charge_char = "↑",
                        discharge_char = "↓",
                        format = "{percent:2.0%} {char}",
                        padding = 5,
                        fontsize=15
                        ),
                widget.TextBox(
                        text="  📅 ",
                        foreground = colors[6],
                        background = colors[0],
                        padding = 0,
                        fontsize=12
                        ),
                # Date
                widget.Clock(
                        foreground = colors[6],
                        background = colors[0],
                        format = "%a, %b %d"
                        ),
                widget.Sep(
                        linewidth = 0,
                        padding = 10,
                        foreground = colors[0],
                        background = colors[0]
                        ),
                widget.TextBox(
                        text=" ⏲",
                        foreground = colors[7],
                        background = colors[0],
                        padding = 0,
                        fontsize=14
                        ),
                # Time
                widget.Clock(
                        foreground = colors[7],
                        background = colors[0],
                        format="%l:%M %p "
                        ),
                widget.Sep(
                        linewidth = 0,
                        padding = 10,
                        foreground = colors[0],
                        background = colors[0]
                        ),
            ],
            size=20, opacity=0.90,
        ),
    ),
]

# Drag floating layouts.
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(), start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(), start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front())
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: List
main = None  # WARNING: this is deprecated and will be removed soon
follow_mouse_focus = False
bring_front_click = False
cursor_warp = False

##### FLOATING WINDOWS #####
floating_layout = layout.Floating(float_rules=[
    {'wmclass': 'confirm'},
    {'wmclass': 'dialog'},
    {'wmclass': 'download'},
    {'wmclass': 'error'},
    {'wmclass': 'file_progress'},
    {'wmclass': 'kcalc'},  # gtk
    {'wmclass': 'notification'},
    {'wmclass': 'splash'},
    {'wmclass': 'toolbar'},
    {'wmclass': 'Bleachbit'},  # gtk
    {'wmclass': 'Clamtk'},  # gtk
    {'wmclass': 'File-roller'},  # gtk
    {'wmclass': 'Nitrogen'},  # gtk
    {'wmclass': 'kruler'},  # gtk
    {'wmclass': 'Hearts'},  # gtk
    {'wmclass': 'Lxappearance'},  # gtk
    {'wmclass': 'Mate-calc'},  # gtk
    {'wmclass': 'Meeting Alert'},  # gtk
    {'wmclass': 'Mousepad'},  # gtk
    {'wmclass': 'PockerTH'},  # gtk
    {'wmclass': 'Qalculate'},  # gtk
    {'wmclass': 'Qalculate-gtk'},  # gtk
    {'wmclass': 'Galculator'},  # gtk
    {'wmclass': 'Gcolor3'},  # gtk
    {'wmclass': 'Gparted'},  # gtk
    {'wmclass': 'Grsync'},  # gtk
    {'wmclass': 'Software-properties-gtk'},  # gtk
    {'wmclass': 'SpeedCrunch'},  # gtk
    {'wmclass': 'Termite'},  # gtk
    {'wmclass': 'Tilix'},  # gtk
    {'wmclass': 'Timeshift'},  # gtk
    {'wmclass': 'Timeshift-gtk'},  # gtk
    {'wmclass': 'Variety'},
    {'wmclass': 'Xfce4-appfinder'},
    {'wname': 'branchdialog'},  # gitk
    {'wname': 'pinentry'},  # GPG key password entry
    {'wmclass': 'ssh-askpass'},  # ssh-askpass
])
auto_fullscreen = True
focus_on_window_activation = "smart"

##### STARTUP APPLICATIONS #####
@hook.subscribe.startup_once
def start_once():
    home = os.path.expanduser('~')
    subprocess.call([home + '/.config/qtile/autostart.sh'])

# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, GitHub issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
#wmname = "LG3D"
wmname = "Qtile"
