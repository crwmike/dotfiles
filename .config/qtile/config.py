import os
import re
import socket
import subprocess
from typing import List  # noqa: F401
from libqtile import layout, bar, widget, hook, qtile
from libqtile.config import Click, Drag, Group, Key, Match, Screen, Rule
from libqtile.lazy import lazy
from libqtile.widget import Spacer
from libqtile.config import ScratchPad, DropDown
#import arcobattery

#mod4 or mod = super key
mod = "mod4"
mod1 = "alt"
mod2 = "control"
home = os.path.expanduser('~')


@lazy.function
def window_to_prev_group(qtile):
    if qtile.currentWindow is not None:
        i = qtile.groups.index(qtile.currentGroup)
        qtile.currentWindow.togroup(qtile.groups[i - 1].name)

@lazy.function
def window_to_next_group(qtile):
    if qtile.currentWindow is not None:
        i = qtile.groups.index(qtile.currentGroup)
        qtile.currentWindow.togroup(qtile.groups[i + 1].name)

keys = [

# Most of our keybindings are in sxhkd file - except these

# SUPER + FUNCTION KEYS

    Key([mod], "f", lazy.window.toggle_fullscreen()),
    #Key([mod], "q", lazy.window.kill()),
    Key([mod, "shift"], "c", lazy.window.kill()),


# SUPER + SHIFT KEYS

    Key([mod, "control"], "q", lazy.window.kill()),
    Key([mod, "control"], "r", lazy.restart()),


# QTILE LAYOUT KEYS
    Key([mod], "n", lazy.layout.normalize()),
    Key(["mod1"], "Tab", lazy.next_layout()),

# CHANGE FOCUS
    Key([mod], "Up", lazy.layout.up()),
    Key([mod], "Down", lazy.layout.down()),
    Key([mod], "k", lazy.layout.up()),
    Key([mod], "j", lazy.layout.down()),
    Key([mod], "h", lazy.layout.left()),
    Key([mod], "l", lazy.layout.right()),


# RESIZE UP, DOWN, LEFT, RIGHT
    Key([mod, "control"], "l",
        lazy.layout.grow_right(),
        lazy.layout.grow(),
        lazy.layout.increase_ratio(),
        lazy.layout.delete(),
        ),
    Key([mod, "control"], "h",
        lazy.layout.grow_left(),
        lazy.layout.shrink(),
        lazy.layout.decrease_ratio(),
        lazy.layout.add(),
        ),
    Key([mod, "control"], "k",
        lazy.layout.grow_up(),
        lazy.layout.grow(),
        lazy.layout.decrease_nmaster(),
        ),
    Key([mod, "control"], "j",
        lazy.layout.grow_down(),
        lazy.layout.shrink(),
        lazy.layout.increase_nmaster(),
        ),


# FLIP LAYOUT FOR MONADTALL/MONADWIDE
    Key([mod, "shift"], "space", lazy.layout.flip()),

# FLIP LAYOUT FOR BSP
    Key([mod, "mod1"], "k", lazy.layout.flip_up()),
    Key([mod, "mod1"], "j", lazy.layout.flip_down()),
    Key([mod, "mod1"], "l", lazy.layout.flip_right()),
    Key([mod, "mod1"], "h", lazy.layout.flip_left()),

# MOVE WINDOWS UP OR DOWN BSP LAYOUT
    Key([mod, "control"], "Down",
        lazy.layout.grow_down(),
        lazy.layout.shrink(),
        lazy.layout.increase_nmaster(),
        ),
#    Key([mod, "shift"], "k", lazy.layout.shuffle_up()),
#    Key([mod, "shift"], "j", lazy.layout.shuffle_down()),
#    Key([mod, "shift"], "h", lazy.layout.shuffle_left()),
#    Key([mod, "shift"], "l", lazy.layout.shuffle_right()),

# MOVE WINDOWS UP OR DOWN MONADTALL/MONADWIDE LAYOUT
    Key([mod, "shift"], "Up", lazy.layout.shuffle_up()),
    Key([mod, "shift"], "Down", lazy.layout.shuffle_down()),
    Key([mod, "shift"], "Left", lazy.layout.swap_left()),
    Key([mod, "shift"], "Right", lazy.layout.swap_right()),

# TOGGLE FLOATING LAYOUT
    Key([mod, "shift"], "f", lazy.window.toggle_floating()),

    ]

def window_to_previous_screen(qtile, switch_group=False, switch_screen=False):
    i = qtile.screens.index(qtile.current_screen)
    if i != 0:
        group = qtile.screens[i - 1].group.name
        qtile.current_window.togroup(group, switch_group=switch_group)
        if switch_screen == True:
            qtile.cmd_to_screen(i - 1)

def window_to_next_screen(qtile, switch_group=False, switch_screen=False):
    i = qtile.screens.index(qtile.current_screen)
    if i + 1 != len(qtile.screens):
        group = qtile.screens[i + 1].group.name
        qtile.current_window.togroup(group, switch_group=switch_group)
        if switch_screen == True:
            qtile.cmd_to_screen(i + 1)

keys.extend([
    # MOVE WINDOW TO NEXT SCREEN
    Key([mod,"shift"], "Right", lazy.function(window_to_next_screen, switch_screen=True)),
    Key([mod,"shift"], "Left", lazy.function(window_to_previous_screen, switch_screen=True)),
])

groups = []

# FOR QWERTY KEYBOARDS
group_names = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "0",]

# FOR AZERTY KEYBOARDS
#group_names = ["ampersand", "eacute", "quotedbl", "apostrophe", "parenleft", "section", "egrave", "exclam", "ccedilla", "agrave",]

group_labels = ["1 ", "2 ", "3 ", "4 ", "5 ", "6 ", "7 ", "8 ", "9 ", "0",]
#group_labels = ["", "", "", "", "", "", "", "", "", "",]
#group_labels = ["Web", "Edit/chat", "Image", "Gimp", "Meld", "Video", "Vb", "Files", "Mail", "Music",]

group_layouts = ["monadtall", "monadtall", "monadtall", "monadtall", "monadtall", "monadtall", "monadtall", "monadtall", "monadtall", "monadtall",]
#group_layouts = ["monadtall", "matrix", "monadtall", "bsp", "monadtall", "matrix", "monadtall", "bsp", "monadtall", "monadtall",]

for i in range(len(group_names)):
    groups.append(
        Group(
            name=group_names[i],
            layout=group_layouts[i].lower(),
            label=group_labels[i],
        ))

for i in groups:
    keys.extend([

#CHANGE WORKSPACES
        Key([mod], i.name, lazy.group[i.name].toscreen()),
        Key([mod], "Right", lazy.screen.next_group()),
        Key([mod], "Left", lazy.screen.prev_group()),

# MOVE WINDOW TO SELECTED WORKSPACE 1-10 AND STAY ON WORKSPACE
        #Key([mod, "shift"], i.name, lazy.window.togroup(i.name)),
# MOVE WINDOW TO SELECTED WORKSPACE 1-10 AND FOLLOW MOVED WINDOW TO WORKSPACE
        Key([mod, "shift"], i.name, lazy.window.togroup(i.name) , lazy.group[i.name].toscreen())
    ])

#####  SCRATCHPADS  #####
groups.append(
    ScratchPad("scratchpad", [
        DropDown("1", "kitty",                              x=0.25, y=0.25, width=0.5,      height=0.6,  opacity=1.0),
        DropDown("2", "pluma Documents/notepad.md",         x=0.25, y=0.25, width=0.5,      height=0.5,  opacity=1.0),
        DropDown("3", "kitty -e qkeys -h",                  x=0.25, y=0.1,  width=0.48,     height=0.74, opacity=1.0),
        DropDown("4", "speedcrunch",                        x=0.60, y=0.40, width=0.35,     height=0.4,  opacity=1.0)
    ])
)

keys.extend([
        Key(["control", "shift"], "a", lazy.group["scratchpad"].dropdown_toggle("1")),
        Key(["control", "shift"], "w", lazy.group["scratchpad"].dropdown_toggle("2")),
        Key([mod               ], "s", lazy.group["scratchpad"].dropdown_toggle("3")),
        Key([mod,     "control"], "c", lazy.group["scratchpad"].dropdown_toggle("4"))
])

def init_layout_theme():
    return {"margin":5,
            "border_width":1,
            "border_focus": "#8aadf4",
            "border_normal": "#5f5faf"
            }

layout_theme = init_layout_theme()


layouts = [
    layout.MonadTall(single_border_width=0, new_at_current=True, single_margin=0, margin=8, border_width=1, border_focus="#8aadf4", border_normal="#5f5faf"),
    layout.MonadWide(single_border_width=0, margin=8, single_margin=0, border_width=1, new_at_current=True, border_focus="#8aadf4", border_normal="#5f5faf"),
    layout.Matrix(**layout_theme),
    #layout.Bsp(**layout_theme),
    #layout.Floating(margin=8, border_width=1, border_focus="#8aadf4", border_normal="#303446"),
    layout.RatioTile(**layout_theme),
    layout.Max(single_border_width=0, single_margin=0, **layout_theme)
]

# COLORS FOR THE BAR
#Theme name : ArcoLinux Default
def init_colors():
    return [["#414559", "#414559"], # color 0
            ["#1e1e2e", "#1e1e2e"], # color 1   background
            ["#5f5f87", "#5f5f87"], # color 2   separator
            ["#eed49f", "#eed49f"], # color 3   yellow
            ["#56bdc2", "#56bdc2"], # color 4   cyan
            ["#f3f4f5", "#f3f4f5"], # color 5   bright white
            ["#f5bde6", "#f5bde6"], # color 6   magenta
            ["#6dffd7", "#6dffd7"], # color 7   green
            ["#8aadf4", "#8aadf4"], # color 8   blue
            ["#a9a9a9", "#a9a9a9"]] # color 9   gray


colors = init_colors()


# WIDGETS FOR THE BAR

def init_widgets_defaults():
    return dict(font="Noto Sans",
                fontsize = 12,
                padding = 2,
                background=colors[1])

widget_defaults = init_widgets_defaults()

def init_widgets_list():
    prompt = "{0}@{1}: ".format(os.environ["USER"], socket.gethostname())
    widgets_list = [
               widget.GroupBox(font="FontAwesome",
                        fontsize = 16,
                        margin_y = -1,
                        margin_x = 0,
                        padding_y = 6,
                        padding_x = 5,
                        borderwidth = 0,
                        disable_drag = True,
                        active = colors[5],
                        inactive = colors[9],
                        rounded = False,
                        # Method of highlighting ('border', 'block', '*text', or 'line')Uses *_border color settings
                        highlight_method = "text",
                        this_current_screen_border = colors[7],
                        foreground = colors[2],
                        background = colors[1]
                        ),
               widget.Sep(
                        linewidth = 1,
                        padding = 20,
                        foreground = colors[2],
                        background = colors[1]
                        ),
               widget.CurrentLayout(
                        font = "Noto Sans Bold",
                        foreground = colors[7],
                        background = colors[1]
                        ),
               widget.Sep(
                        linewidth = 1,
                        padding = 20,
                        foreground = colors[2],
                        background = colors[1]
                        ),
               #widget.WindowName(font="JetBrainsMono Nerd Font",
               #         fontsize = 14,
               #         foreground = colors[5],
               #         background = colors[1],
               #         ),
                widget.WindowTabs(
                        font="JetBrainsMono Nerd Font",
                        fontsize = 14,
                        foreground = colors[5],
                        background = colors[1],
                        separator = ' :: '
                        ),
               widget.CheckUpdates(
                        distro = 'Arch_checkupdates',
                        font="JetBrainsMono Nerd Font Bold",
                        no_update_string='',
                        colour_no_updates = colors[5],
                        colour_have_updates = colors[3]
                        ),
               widget.Sep(
                        linewidth = 1,
                        padding = 20,
                        foreground = colors[2],
                        background = colors[1]
                        ),
               widget.TextBox(
                        font="FontAwesome",
                        text="",
                        foreground=colors[6],
                        background=colors[1],
                        padding = 0,
                        fontsize=18
                        ),
               widget.Volume(
                        fmt = ' Vol: {}',
                        foreground = colors[5],
                        background = colors[1],
                        padding = 5
                        ),
               widget.Sep(
                        linewidth = 1,
                        padding = 20,
                        foreground = colors[2],
                        background = colors[1]
                        ),
               widget.TextBox(
                        font="FontAwesome",
                        text="  ",
                        foreground=colors[8],
                        background=colors[1],
                        padding = 0,
                        fontsize=16
                        ),
               widget.Battery(
                        font="JetBrainsMono Nerd Font",
                        format = "Bat: {char} {percent:1.0%}",
                        update_interval = 10,
                        fontsize = 14,
                        charge_char = "▲",
                        discharge_char = "▼",
                        foreground = colors[5],
                        background = colors[1],
                        low_percentage = 0.05,
                        low_forground = colors[6]
                        ),
               widget.Sep(
                        linewidth = 1,
                        padding = 20,
                        foreground = colors[2],
                        background = colors[1]
                        ),
               widget.TextBox(
                        font="FontAwesome",
                        text="  ",
                        foreground=colors[4],
                        background=colors[1],
                        padding = 0,
                        fontsize=14
                        ),
               widget.Clock(
                        font="JetBrainsMono Nerd Font Bold",
                        foreground = colors[5],
                        background = colors[1],
                        fontsize = 14,
                        #format="%Y-%m-%d %H:%M"
                        format="%a, %b %d, %Y"
                        ),
               widget.Sep(
                        linewidth = 1,
                        padding = 20,
                        foreground = colors[2],
                        background = colors[1]
                        ),
               widget.TextBox(
                        font="FontAwesome",
                        text=" ",
                        foreground=colors[7],
                        background=colors[1],
                        padding = 0,
                        fontsize=17
                        ),
               widget.Clock(
                        font="JetBrainsMono Nerd Font Bold",
                        foreground = colors[5],
                        background = colors[1],
                        fontsize = 15,
                        #format="%Y-%m-%d %H:%M"
                        format="%l:%M %p "
                        #format="%H:%M "
                        ),
               widget.Sep(
                        linewidth = 1,
                        padding = 10,
                        foreground = colors[2],
                        background = colors[1]
                        ),
               widget.Systray(
                        background=colors[1],
                        icon_size=20,
                        padding = 4
                        ),
              ]
    return widgets_list

widgets_list = init_widgets_list()


def init_widgets_screen1():
    widgets_screen1 = init_widgets_list()
    return widgets_screen1

def init_widgets_screen2():
    widgets_screen2 = init_widgets_list()
    return widgets_screen2

widgets_screen1 = init_widgets_screen1()
widgets_screen2 = init_widgets_screen2()


def init_screens():
    return [Screen(top=bar.Bar(widgets=init_widgets_screen1(), size=26, opacity=0.8)),
            Screen(top=bar.Bar(widgets=init_widgets_screen2(), size=26, opacity=0.8))]
screens = init_screens()


# MOUSE CONFIGURATION
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(),
         start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(),
         start=lazy.window.get_size())
]

dgroups_key_binder = None
dgroups_app_rules = []

# ASSIGN APPLICATIONS TO A SPECIFIC GROUPNAME
# BEGIN

#########################################################
################ assgin apps to groups ##################
#########################################################
# @hook.subscribe.client_new
# def assign_app_group(client):
#     d = {}
#     #####################################################################################
#     ### Use xprop fo find  the value of WM_CLASS(STRING) -> First field is sufficient ###
#     #####################################################################################
#     d[group_names[0]] = ["Navigator", "Firefox", "Vivaldi-stable", "Vivaldi-snapshot", "Chromium", "Google-chrome", "Brave", "Brave-browser",
#               "navigator", "firefox", "vivaldi-stable", "vivaldi-snapshot", "chromium", "google-chrome", "brave", "brave-browser", ]
#     d[group_names[1]] = [ "Atom", "Subl", "Geany", "Brackets", "Code-oss", "Code", "TelegramDesktop", "Discord",
#                "atom", "subl", "geany", "brackets", "code-oss", "code", "telegramDesktop", "discord", ]
#     d[group_names[2]] = ["Inkscape", "Nomacs", "Ristretto", "Nitrogen", "Feh",
#               "inkscape", "nomacs", "ristretto", "nitrogen", "feh", ]
#     d[group_names[3]] = ["Gimp", "gimp" ]
#     d[group_names[4]] = ["Meld", "meld", "org.gnome.meld" "org.gnome.Meld" ]
#     d[group_names[5]] = ["Vlc","vlc", "Mpv", "mpv" ]
#     d[group_names[6]] = ["VirtualBox Manager", "VirtualBox Machine", "Vmplayer",
#               "virtualbox manager", "virtualbox machine", "vmplayer", ]
#     d[group_names[7]] = ["Thunar", "Nemo", "Caja", "Nautilus", "org.gnome.Nautilus", "Pcmanfm", "Pcmanfm-qt",
#               "thunar", "nemo", "caja", "nautilus", "org.gnome.nautilus", "pcmanfm", "pcmanfm-qt", ]
#     d[group_names[8]] = ["Evolution", "Geary", "Mail", "Thunderbird",
#               "evolution", "geary", "mail", "thunderbird" ]
#     d[group_names[9]] = ["Spotify", "Pragha", "Clementine", "Deadbeef", "Audacious",
#               "spotify", "pragha", "clementine", "deadbeef", "audacious" ]
#     ######################################################################################
#
# wm_class = client.window.get_wm_class()[0]
#
#     for i in range(len(d)):
#         if wm_class in list(d.values())[i]:
#             group = list(d.keys())[i]
#             client.togroup(group)
#             client.group.cmd_toscreen(toggle=False)

# END
# ASSIGN APPLICATIONS TO A SPECIFIC GROUPNAME

main = None

# hides the top bar when the archlinux-logout widget is opened
@hook.subscribe.client_new
def new_client(window):
    if window.name == "ArchLinux Logout":
        qtile.hide_show_bar()

# shows the top bar when the archlinux-logout widget is closed
@hook.subscribe.client_killed
def logout_killed(window):
    if window.name == "ArchLinux Logout":
        qtile.hide_show_bar()

@hook.subscribe.startup_once
def start_once():
    home = os.path.expanduser('~')
    subprocess.call([home + '/.config/qtile/scripts/autostart.sh'])

@hook.subscribe.startup
def start_always():
    # Set the cursor to something sane in X
    subprocess.Popen(['xsetroot', '-cursor_name', 'left_ptr'])

@hook.subscribe.client_new
def set_floating(window):
    if (window.window.get_wm_transient_for()
            or window.window.get_wm_type() in floating_types):
        window.floating = True

floating_types = ["notification", "toolbar", "splash", "dialog"]


follow_mouse_focus = False
bring_front_click = False
cursor_warp = False
floating_layout = layout.Floating(float_rules=[
    # Run the utility of `xprop` to see the wm class and name of an X client.
    *layout.Floating.default_float_rules,
    Match(wm_class='confirmreset'),  # gitk
    Match(wm_class='makebranch'),  # gitk
    Match(wm_class='maketag'),  # gitk
    Match(wm_class='ssh-askpass'),  # ssh-askpass
    Match(title='branchdialog'),  # gitk
    Match(title='pinentry'),  # GPG key password entry
    Match(wm_class='Arcolinux-welcome-app.py'),
    Match(wm_class='Arcolinux-calamares-tool.py'),
    Match(wm_class='confirm'),
    Match(wm_class='dialog'),
    Match(wm_class='download'),
    Match(wm_class='error'),
    Match(wm_class='file_progress'),
    Match(wm_class='kruler'),
    Match(wm_class='notification'),
    Match(wm_class='Nitrogen'),
    Match(wm_class='splash'),
    Match(wm_class='speedcrunch'),
    Match(wm_class='Timeshift-gtk'),
    Match(wm_class='toolbar'),
    Match(wm_class='Arandr'),
    Match(wm_class='feh'),
    Match(wm_class='Galculator'),
    Match(wm_class='Variety'),
    Match(wm_class='Xfce4-appfinder'),
    Match(wm_class='archlinux-logout'),
    Match(wm_class='xfce4-terminal'),

],  fullscreen_border_width = 0, 
    border_width = 1,
    border_focus="#8aadf4",
    border_normal="#303446")
auto_fullscreen = True

focus_on_window_activation = "focus" # or smart

wmname = "Qtile"
