# Sublime Text 3

## Copy settings to a new machine.

Read more at [packagecontrol.io](https://packagecontrol.io/docs/syncing).

### Linux

If your Dropbox folder is not in the default location, you'll need to change ~/Dropbox to your location.

1. Close Sublime Text
2. Open Terminal

### First Machine

On your first machine, use the following instructions.

    cd ~/.config/sublime-text-3/Packages/
    mkdir ~/Dropbox/Sublime
    mv User ~/Dropbox/Sublime/
    ln -s ~/Dropbox/Sublime/User

### Other Machine(s)

On your other machine(s), use the following instructions. __These instructions will remove your User/ folder and all contents!__

    cd ~/.config/sublime-text-3/Packages/
    rm -r User
    ln -s ~/Dropbox/Sublime/User

![screenshot](https://gitlab.com/crwmike/dotfiles/raw/master/.config/sublime-text-3/sublime-ss.jpg)