#
# Powerline Cyan Block - Tmux Theme
# Created by Jim Myhrberg <contact@jimeh.me>.
#
# Inspired by vim-powerline: https://github.com/Lokaltog/powerline
#
# Requires terminal to be using a powerline compatible font, find one here:
# https://github.com/Lokaltog/powerline-fonts
#

# Status update interval
set -g status-interval 1

# Basic status bar colors
set -g status-style fg=colour250,bg=colour235

# Left side of status bar
set -g status-left-style bg=colour233,fg=colour243
set -g status-left-length 40
set -g status-left "#[fg=colour232,bg=colour141,bold] #S #[fg=colour141,bg=colour240,nobold]#[fg=colour251,bg=colour239] #(whoami) #[fg=colour240,bg=colour235]#[fg=colour251,bg=colour237] #I:#P #[fg=colour235,bg=colour233,nobold]"

# Right side of status bar
set -g status-right-style bg=colour233,fg=colour243
set -g status-right-length 150
set -g status-right "#[fg=colour235,bg=colour233]#[fg=colour251,bg=colour237] %H:%M:%S #[fg=colour240,bg=colour235]#[fg=colour251,bg=colour239] %d-%b-%y #[fg=colour61,bg=colour240]#[fg=colour232,bg=colour141,bold] #H "

# Window status
set -g window-status-format " #I:#W#F "
set -g window-status-current-format " #I:#W#F "

# Current window status
set -g window-status-current-style bg=colour240,fg=colour255

# Window with activity status
set -g window-status-activity-style bg=colour237,fg=colour75

# Window separator
set -g window-status-separator ""

# Window status alignment
set -g status-justify centre

# Pane border
set -g pane-border-style bg=default,fg=colour238

# Active pane border
set -g pane-active-border-style bg=default,fg=colour141

# Pane number indicator
set -g display-panes-colour colour237
set -g display-panes-active-colour colour255

# Clock mode
set -g clock-mode-colour colour141
set -g clock-mode-style 24

# Message
set -g message-style bg=colour141,fg=black

# Command message
set -g message-command-style bg=colour233,fg=black

# Mode
set -g mode-style bg=colour141,fg=colour232

