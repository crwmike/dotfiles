# TMUX config in ~/.config/tmux

### In ~/.bashrc or ~/.bash_aliases add the following:

    alias tmux='tmux -f ~/.config/tmux/tmux.conf'

### Save and reload shell.


## tmux keys

* leader Ctrl+A


### split panes

* C-a \       split right
* C-a -       split down


### switch panes

* M-Left      move to the pane to the left
* M-Right     move to the pane to the right
* M-Up        move to the pane above
* M-Down      move to the pane below

### reload config file

    C-a r


