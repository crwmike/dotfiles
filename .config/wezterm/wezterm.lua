--                       __
-- .--.--.--.-----.-----|  |_.-----.----.--------.
-- |  |  |  |  -__|-- __|   _|  -__|   _|        |
-- |________|_____|_____|____|_____|__| |__|__|__|
--
-- Pull in the wezterm API
local wezterm = require 'wezterm'

-- This will hold the configuration.
local config = wezterm.config_builder()

-- This is where you actually apply your config choices
config.window_background_opacity = 0.86
config.font = wezterm.font("JetBrainsMono Nerd Font")
config.font_size = 10.0
config.scrollback_lines = 3500
-- Color Schemes
--      'Catppuccin Macchiato'      'Catppuccin Mocha'
--      'catppuccin-macchiato'      'catppuccin-mocha'
--      'Everforest Dark (Gogh)'    'Everforest Dark Medium (Gogh)'
--      'Palenight (Gogh)'
--      'rose-pine'                 'rose-pine-moon'
--      'Tokyo Night'               'Tokyo Night Storm'
config.color_scheme = 'Catppuccin Mocha'
-- Cursor Style -- Acceptable values are:
--      SteadyBlock         BlinkingBlock
--      SteadyUnderline     BlinkingUnderline
--      SteadyBar           BlinkingBar
config.default_cursor_style = "BlinkingBar"

-- -----------------------------------------------------------------------------
-- KEY BINDINGS
-- -----------------------------------------------------------------------------
config.keys = {
  {
    key = '-',
    mods = 'ALT|CTRL',
    action = wezterm.action.SplitVertical { domain = 'CurrentPaneDomain' },
  },
  {
    key = '0',
    mods = 'ALT|CTRL',
    action = wezterm.action.SplitHorizontal { domain = 'CurrentPaneDomain' },
  },
  {
    key = 'Tab',
    mods = 'CTRL',
    action = wezterm.action{ActivateTabRelative=1},
  },
  {
    key = 'Tab',
    mods = 'CTRL|SHIFT',
    action = wezterm.action{ActivateTabRelative=-1},
  },
}

-- -----------------------------------------------------------------------------
-- TAB BAR
-- -----------------------------------------------------------------------------
config.enable_tab_bar = true
config.hide_tab_bar_if_only_one_tab = true
config.tab_bar_at_bottom = false

config.window_frame = {
  -- The font used in the tab bar. 
  -- Roboto Bold is the default; this font is bundled with wezterm. Whatever font 
  -- is selected here, it will have the main font setting appended to it to pick 
  -- up any fallback fonts you may have used there.
  font = wezterm.font { family = 'JetBrainsMono', weight = 'Bold' },

  -- The size of the font in the tab bar.
  font_size = 10.0,

  -- #1e1e2e Mocha
  -- #24273a Macchiato
  -- #303446 Frappe
  --
  -- The overall background color of the tab bar when the window is focused.
  active_titlebar_bg = '#1e1e2e',

  -- The overall background color of the tab bar when the window is not focused.
  inactive_titlebar_bg = '#303446',
}

config.colors = {
  tab_bar = {
    -- The color of the inactive tab bar edge/divider
    inactive_tab_edge = '#000000',
  },
}

-- -----------------------------------------------------------------------------
-- and finally, return the configuration to wezterm
-- -----------------------------------------------------------------------------
return config
