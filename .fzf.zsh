# Setup fzf
# ---------
if [[ ! "$PATH" == */home/mike/.fzf/bin* ]]; then
  PATH="${PATH:+${PATH}:}/home/mike/.fzf/bin"
fi

# Auto-completion
# ---------------
source "/home/mike/.fzf/shell/completion.zsh"

# Key bindings
# ------------
source "/home/mike/.fzf/shell/key-bindings.zsh"
