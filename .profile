# ~/.profile: executed by the command interpreter for login shells.
# This file is not read by bash(1), if ~/.bash_profile or ~/.bash_login
# exists.
# see /usr/share/doc/bash/examples/startup-files for examples.
# the files are located in the bash-doc package.

# the default umask is set in /etc/profile; for setting the umask
# for ssh logins, install and configure the libpam-umask package.
#umask 022

# if running bash
if [ -n "$BASH_VERSION" ]; then
    # include .bashrc if it exists
    if [ -f "$HOME/.bashrc" ]; then
    . "$HOME/.bashrc"
    fi
fi

# set PATH so it includes user's private bin if it exists
if [ -d "$HOME/bin" ] ; then
    PATH="$HOME/bin:$PATH"
fi

###########################
##  User added settings  ##
###########################

# qt apps use gnome themes
# sudo apt install qt5-style-kvantum
export QT_STYLE_OVERRIDE=kvantum

# brightnessctl
source /home/mike/.local/bin/brightnessctl/configuration.sh

echo ""

PS1='${debian_chroot:+($debian_chroot)}\[\033[01;36m\]\u\033[1;35m@\033[1;32m\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[0;36m\]$(__git_ps1)\[\033[00m\]\$ '
PS2='\033[01;34mcontinue\033[01;32m-> \033[00m'
PS3='\033[01;32m--> \033[00m'
PS4='\033[01;34m\+ \033[00m'

export TERM='xterm-256color'
if [ -d "/home/linuxbrew" ] ; then
    PATH="$PATH:/home/linuxbrew/.linuxbrew/bin:$HOME/.cargo/bin"
fi

#############################
## source .profile, .zshrc ##
#############################
echo -e "source (\e[33m.profile, .zshrc\e[0m)"
export PATH="$PATH:$HOME/.local/bin"
export QT_QPA_PLATFORMTHEME=gtk2
. "$HOME/.cargo/env" 2>/dev/null
echo
###########################
## MOTD - login terminal ##
###########################
# export TERMINAL=tilix
tput setaf 14
date +"%a %b %C" | figlet
tput sgr0
echo
fortune | batcat -n
echo


source /home/mike/.config/broot/launcher/bash/br
