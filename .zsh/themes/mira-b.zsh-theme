# Based on bira theme

setopt prompt_subst

() {

local PR_USER PR_USER_OP PR_PROMPT PR_HOST

# Check the UID
if [[ $UID -ne 0 ]]; then # normal user
  PR_USER='%F{141}%n%f'
  PR_USER_OP='%F{141}%#%f'
  PR_PROMPT='%f➤ %f'
else # root
  PR_USER='%F{red}%n%f'
  PR_USER_OP='%F{red}%#%f'
  PR_PROMPT='%F{red}➤ %f'
fi

# Check if we are on SSH or not
if [[ -n "$SSH_CLIENT"  ||  -n "$SSH2_CLIENT" ]]; then
  PR_HOST='%F{red}%M%f' # SSH
else
  PR_HOST='%F{141}%M%f' # no SSH
fi

# Show vim mode
function zle-line-init zle-keymap-select {
    RPS1="%F{84}%${${KEYMAP/vicmd/ <<< vicmd >>> }/(main|viins)/}"
    RPS2=$RPS1
    zle reset-prompt
}

zle -N zle-line-init
zle -N zle-keymap-select

local return_code="%(?..%F{red}%? ↵%f)"

local user_host="${PR_USER}%F{78}@${PR_HOST}"
local current_dir="%B%F{blue}%~%f%b%f"
local git_branch='$(git_prompt_info)'

PROMPT="%F{215}╭─${user_host} ${current_dir} \$(ruby_prompt_info) ${git_branch}
%F{215}╰─$PR_PROMPT "
RPROMPT="${return_code}"

ZSH_THEME_GIT_PROMPT_PREFIX="%F{117}‹"
ZSH_THEME_GIT_PROMPT_DIRTY="%F{204}✗"
ZSH_THEME_GIT_PROMPT_SUFFIX="%F{cyan}› %f"

ZSH_THEME_RUBY_PROMPT_PREFIX="%F{red}‹"
ZSH_THEME_RUBY_PROMPT_SUFFIX="›%f"

}
