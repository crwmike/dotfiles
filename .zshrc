#-------------------------------------------------------------------------------
# Script Name:      .zshrc
# Description:      zsh config rc file
# Dependencies:     zsh
# GitLab:           https://www.gitlab.com/crwmike/dotfiles
# Contributors:     Michael Grosen
#-------------------------------------------------------------------------------
# ----  ZSH CONFIG                                                          ----
#-------------------------------------------------------------------------------
export ZSH=$HOME/gitlab/dotfiles/.config/zsh
# ------------------------------------------------------------------------------
# ---- ZSH PLUGINS (Add wisely, too many plugins slow down shell startup.)  ----
# ------------------------------------------------------------------------------
source $ZSH/plugin/fancy-ctrl-z/fancy-ctrl-z.plugin.zsh
source $ZSH/plugin/vi-mode/vi-mode.plugin.zsh
source $ZSH/plugin/zsh-autosuggestions/zsh-autosuggestions.zsh
source $ZSH/plugin/zsh-autocomplete/zsh-autocomplete.plugin.zsh
source $ZSH/plugin/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
source $ZSH/plugin/zsh-you-should-use/you-should-use.plugin.zsh
# ------------------------------------------------------------------------------
skip_global_compinit=1
#### Make <kbd>Tab</kbd> go straight to the menu and cycle there
bindkey '\t' menu-select "$terminfo[kcbt]" menu-select
#bindkey -M menuselect '\t' menu-complete "$terminfo[kcbt]" reverse-menu-complete

# ------------------------------------------------------------------------------
# ---- User added content below.                                            ----
# ---- For user aliases, Use the file .aliases                              ----
# ------------------------------------------------------------------------------
alias loadrc="echo 'loading zsh config' && source ~/.zshrc"
stty -ixon            # disable ctrl-s and ctrl-q

# load ~/.aliases
if [ -f ~/.aliases ]; then
    . ~/.aliases
fi

### swap caps_lock and esc, caps off
#xmodmap ~/.Xmodmap
setxkbmap -option ctrl:nocaps

bindkey '^Z' fancy-ctrl-z                   # install plugin fancy-ctrl-z

### for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
### don't put duplicate lines or lines starting with space in the history.
HISTFILE=~/.zsh_history
HISTCONTROL=ignoreboth
HISTSIZE=1000000000
HISTFILESIZE=2000000000

### window title
DISABLE_AUTO_TITLE="true"
case $TERM in xterm*)
    precmd () {print -Pn "\e]0;%~\a"}
    ;;
esac

# ------------------------------------------------------------------------------
# ---- color less-manpages                                                  ----
# ------------------------------------------------------------------------------
export LESS_TERMCAP_md=$(printf '\e[38;5;111m')  # blink mode     (section)
export LESS_TERMCAP_so=$(printf '\e[0;30;46m')   # reverse standout mode
export LESS_TERMCAP_se=$(printf '\e[0m')         # leave standout mode
export MANPAGER="less -R --use-color -Dd+b -Du+m"
export MANROFFOPT="-P -c"
export MANWIDTH=80
export LESSHISTFILE="/home/mike/.config/less/history"
#-------------------------------------------------------------------------------
# ---- more system variables                                                ----
#-------------------------------------------------------------------------------
export NANOCONF='/usr/share/nano'
export BROWSER='vivaldi-stable'
export BROWSERCLI='/usr/bin/lynx'           # terminal browsers: elinks, lynx, w3m
export LYNX_CFG="$HOME/.config/lynx/lynx.cfg"
export LYNX_LSS="$HOME/.config/lynx/lynx.lss"
export WWW_HOME='https://duckduckgo.com'
export RANGER_LOAD_DEFAULT_RC=false
export LF_ICONS="ln=:or=:tw=ω:ow=:st=σ:di=:pi=π:so=φ:bd=β:cd=ξ:su=ψ:sg=γ:ex=:fi="
export PAGER='less -Xi'
export EDITOR='/usr/bin/nvim'
export SUDO_EDITOR='/usr/bin/nvim'
export VISUAL='/usr/bin/nvim'
export TERMINAL='kitty'
export BAT_THEME='Catppuccin-macchiato'
export PF_INFO="ascii title os host kernel uptime pkgs shell"
export GREP_COLORS='ms=33:fn=35:ln=32'
export LS_COLORS=$LS_COLORS:"ex=38;5;86:di=38;5;111:ln=0;38;5;117"
export EXA_COLORS='di=38;5;111:hd=4;37:oc=36:in=37:ln=38;5;117:ur=32:uw=32:ux=32:gr=34:gw=34:gx=34:tr=35:tw=35:tx=35:sn=38;5;224:sb=37:lc=37:lm=1;35:uu=38;5;183:gu=38;5;183:gn=33:un=33:da=38;5;37'
export WEATHER_CLI_API=9011681d935bf876c22bcef577c97102
export MICRO_TRUECOLOR=1
export WEECHAT_HOME=~/.config/weechat
export HIGHLIGHT_STYLE=candy
### directory vars
export BIN="$HOME/bin"
export CFG="$HOME/.config"
export LBIN="$HOME/.local/bin"
export DL="$HOME/Downloads"
export DOC="$HOME/Documents/"
export ISO="$HOME/Downloads/iso"
export SRC="$HOME/src/"
export TMP="$HOME/tmp/"
### FFF config 1:directory 2:status 3:selection 4:cursor
export FFF_COL2=6
export FFF_COL3=3
export FFF_COL4=5
### NNN config
export NNN_USE_EDITOR=1 # nnn: open all text files in EDITOR (fallback vi)
export NNN_OPTS="H"
export NNN_COLORS='5423'
export NNN_FCOLORS='c1df6f96007475fad6dfdad2'
export NNN_BMS='b:~/bin;c:~/.config;d:~/Downloads;D:~/gitlab/dotfiles;e:~/Documents/editor-keys;n:~/.config/nvim;q:~/.config/qtile;r:~/.config/rofi;t:~/Documents;z:~/.oh-my-zsh/custom/themes'
### tuifi
export tuifi_vim_mode=True
# ------------------------------------------------------------------------------
# ---- zsh prompt vi mode                                                   ----
# ------------------------------------------------------------------------------
        # add missing vim hotkeys
        # fix backspace deletions issues
        # http://zshwiki.org/home/zle/vi-mode
bindkey -a u undo
bindkey -a '^R' redo
bindkey '^?' backward-delete-char  #backspace
DISABLE_AUTO_UPDATE=true 
# ------------------------------------------------------------------------------
# ---- FZF - A CLI fuzzy finder                                             ----
# ------------------------------------------------------------------------------
###     CTRL-T -  Paste the selected files and directories onto the command line
###     CTRL-R -  Paste the selected command from history onto the command line
###     ALT-D  -  cd into the selected directory
[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh
export FZF_DEFAULT_OPS="--extended"
### ALT-D - cd  (originally ALT-C)
bindkey '\ed' fzf-cd-widget
### fzf-cd-widget: fix zle error
TRAPWINCH() {
  zle && { zle reset-prompt; zle -R }
}
#source /usr/share/doc/fzf/examples/key-bindings.zsh
source /usr/share/zsh/plugins/forgit/forgit.plugin.zsh
# ------------------------------------------------------------------------------
# ---- forgit plugin for fzf and git                                        ----
# ------------------------------------------------------------------------------
### git clone https://github.com/wfxr/forgit ~/.forgit
[ -f ~/.forgit/forgit.plugin.zsh ] && source ~/.forgit/forgit.plugin.zsh 2>/dev/null
# ------------------------------------------------------------------------------
### Zsh tab-completion for cd
autoload -U compinit && compinit -u

#PATH
if [[ ":$PATH:" != *":$HOME/bin:"* ]]; then
    export PATH="$PATH:/home/mike/.bin:/home/mike/bin:/home/mike/.local/bin:/home/mike/bin/keyfiles:/home/mike/bin/fetches"
fi

# ------------------------------------------------------------------------------
# ---- Disable touchpad                                                     ----
# ----      super + KP_Add          enable touchpad     xinput enable 10    ----
# ----      super + KP_Subtract     disable touchpad    xinput disable 10   ----
# ------------------------------------------------------------------------------
xinput disable 10
# ------------------------------------------------------------------------------
# ---- load starship prompt                                                 ----
# ------------------------------------------------------------------------------
eval "$(starship init zsh)"
### STARSHIP suppress timeout error
export STARSHIP_LOG=error
# ------------------------------------------------------------------------------
# ---- terminal init                                                        ----
# ------------------------------------------------------------------------------
eval "$(zoxide init zsh)"
### Random colors
# AC=(32 33 34 35 36 92 93 94 95 96)
# echo -en "\e[${AC[RANDOM%${#AC[@]}]}m"
# export COWS=("bunny" "default" "duck" "elephant" "eyes" "hellokitty" "koala" "luke-koala" "milk" "moose" "sheep" "snowman" "suse" "three-eyes" "tux" "www")
# sleep 1
# fortune hitchhickers | cowsay -f ${COWS[RANDOM%${#COWS[@]}]}
# echo -en "\e[0m"
#rweather
#uptime --pretty
checkupdates
#echo
#dfetch
#date '+%A, %B %d, %Y'
task list due.before:tomorrow
remind ~/.reminders

