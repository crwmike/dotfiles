# crwmike/dotfiles

This repository contains my personal dotfiles. They are stored here for convenience so that I may quickly access them on new machines or new installs.

###### qtile
![qtile screenshot](https://gitlab.com/crwmike/dotfiles/raw/master/qtile_ss.jpg)

### Dotfiles

Dotfiles are the customization files that are used to personalize your Linux or other Unix-based system.  

Your unofficial guide to dotfiles on GitHub: [here](https://dotfiles.github.io/)

---

## Use SSH keys to communicate with GitLab

[docs.gitlab.com/ee/user/ssh.html](https://docs.gitlab.com/ee/user/ssh.html)     

### Generate an SSH key pair   

If you do not have an existing SSH key pair, generate a new one:

1. Open a terminal.
2. Run ssh-keygen -t followed by the key type and an optional comment. This comment is included in the .pub file that’s created. You may want to use an email address for the comment.

For example, for ED25519:

```   
ssh-keygen -t ed25519 -C "<comment>"     
```   

For 2048-bit RSA:   

```   
ssh-keygen -t rsa -b 2048 -C "<comment>"  
```   

### Adding a Remote    

```    
git remote set-url origin git@github.com:USERNAME/REPOSITORY.git     
```   

### Add an SSH key to your GitLab account    

To use SSH with GitLab, copy your public key to your GitLab account:    

1. Copy the contents of your public key file. You can do this manually or use a script. For example, to copy an ED25519 key to the clipboard: 

Linux (requires the xclip package)     

```
xclip -sel clip < ~/.ssh/id_ed25519.pub    
```   

2. Sign in to GitLab.  
3. On the left sidebar, select your avatar.  
4. Select Edit profile.   
5. On the left sidebar, select SSH Keys.    
6. Select Add new key.    
7. In the Key box, paste the contents of your public key. If you manually copied the key, make sure you copy the entire key, which starts with ssh-rsa, ssh-dss, ecdsa-sha2-nistp256, ecdsa-sha2-nistp384, ecdsa-sha2-nistp521, ssh-ed25519, sk-ecdsa-sha2-nistp256@openssh.com, or sk-ssh-ed25519@openssh.com, and may end with a comment.    
8. In the Title box, type a description, like Work Laptop or Home Workstation.     
9. Optional. Select the Usage type of the key. It can be used either for Authentication or Signing or both. Authentication & Signing is the default value.    
10. Optional. Update Expiration date to modify the default expiration date.     
11. Select Add key.   

###  Verify that you can connect   

1. To ensure you’re connecting to the correct server, check the server’s SSH host keys fingerprint.     
2. Open a terminal and run this command, replacing gitlab.example.com with your GitLab instance URL:   

```  
ssh -T git@gitlab.example.com   
```  

3. If this is the first time you connect, you should verify the authenticity of the GitLab host. If you see a message like:    

```    
The authenticity of host 'gitlab.example.com (35.231.145.151)' can't be established.
ECDSA key fingerprint is SHA256:HbW3g8zUjNSksFbqTiUWPWg2Bq1x8xdGUrliXFzSnUw.
Are you sure you want to continue connecting (yes/no)? yes
Warning: Permanently added 'gitlab.example.com' (ECDSA) to the list of known hosts.
```   
Type yes and press Enter.

4. Run the ssh -T git@gitlab.example.com command again. You should receive a Welcome to GitLab, @username! message.
If the welcome message doesn’t appear, you can troubleshoot by running ssh in verbose mode:

```   
ssh -Tvvv git@gitlab.example.com   
```   

