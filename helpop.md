HELPON
======

To enable key binding help on startup (Alt-g: bindings), add the following to settings.json.

```
"keymenu": true
```
